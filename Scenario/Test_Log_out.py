import Test_capability
import time
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_capability.setUp()

time.sleep(20)

ele_profile_Btn = "sg.gov.mnd.OneService:id/bottom_navigation_profile"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_profile_Btn)):
        driver.find_element_by_id(ele_profile_Btn).click()
except:
    pass

#Click log out button
time.sleep(5)
ele_logout_Btm = "sg.gov.mnd.OneService:id/user_profile_item_logout"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_logout_Btm)):
        driver.find_element_by_id(ele_logout_Btm).click()
except:
    pass

print("User log out successfully...")

driver.quit()