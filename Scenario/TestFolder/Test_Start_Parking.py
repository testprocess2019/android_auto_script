#!/usr/bin/env python
import Test_capability
import time
from selenium.webdriver.support.ui import WebDriverWait
#from Scenario import XLUtilities
import XLUtilities

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Start_Park_Info.xlsx"


#Navigate to user profile
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile").click()
except:
    pass
#Enter credit card info
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_credit_card_label")):
        elcc = driver.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_credit_card_label")
        elcc.click()
except:
    pass


try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/et_cc_num")):
        elcn = driver.find_element_by_id("sg.gov.mnd.OneService:id/et_cc_num")
        elcn.send_keys('4111111111111111')
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/card_date")):
        elcmmyy = driver.find_element_by_id("sg.gov.mnd.OneService:id/card_date")
        elcmmyy.send_keys('1224')
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/card_cvc")):
        elcvc = driver.find_element_by_id("sg.gov.mnd.OneService:id/card_cvc")
        elcvc.send_keys('123')
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/add_card")):
        elAddcard = driver.find_element_by_id("sg.gov.mnd.OneService:id/add_card")
        elAddcard.click()
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("android:id/button1")):
        elOkAddcard = driver.find_element_by_id("android:id/button1")
        elOkAddcard.click()
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_home")):
        elOkAddcardHm = driver.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_home")
        elOkAddcardHm.click()
except:
    pass


#Launch start parking page:
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/eparking_module_icon")):
        el1 = driver.find_element_by_id("sg.gov.mnd.OneService:id/eparking_module_icon")
        el1.click()
except:
    pass

#Start parking page:

#1. Parking duration:
expected_parking_duration = XLUtilities.readData(path, "Info", 3, 3)
el1 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_duration_mins").get_attribute('text')
print("Actual parking duration: " + el1)
print("Expected packing duration: " + str(expected_parking_duration))
if el1 == expected_parking_duration:
        XLUtilities.writeData(path, "Info", 3, 4, "PASS")
        XLUtilities.writeData(path, "Info", 3, 2, el1)
else:
        XLUtilities.writeData(path, "Info", 3, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 3, 2, el1)

#Select car park
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/parking_carpark")):
        el2 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_carpark")
        el2.click()
except:
    pass

#wait for all nearest car park info displayed
time.sleep(5)

# Select nearest car park
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView")):
        el3 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView")
        el3.click()
except:
    pass
#2. Total cost:
expected_total_cost = XLUtilities.readData(path, "Info", 4, 3)
el2 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_cost").get_attribute('text')
print("Actual total cost: " + el2)
print("Expected total cost: " + str(expected_total_cost))
if el2 == expected_total_cost:
        XLUtilities.writeData(path, "Info", 4, 4, "PASS")
        XLUtilities.writeData(path, "Info", 4, 2, el2)
else:
        XLUtilities.writeData(path, "Info", 4, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 4, 2, el2)

#3. Verifiy start parking page displayed:
expected_parking_header = XLUtilities.readData(path, "Info", 5, 3)
el3 = driver.find_element_by_id("sg.gov.mnd.OneService:id/tvHeader").get_attribute('text')
print("Actual start parking page title: " + el3)
print("Expected start parking page title: " + expected_parking_header)
if el3 == expected_parking_header:
        XLUtilities.writeData(path, "Info", 5, 4, "PASS")
        XLUtilities.writeData(path, "Info", 5, 2, el3)
else:
        XLUtilities.writeData(path, "Info", 5, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 5, 2, el3)

#4. Verifiy select nearest car park:
expected_nearest_parking = XLUtilities.readData(path, "Info", 6, 3)
el4 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_carpark").get_attribute('text')
print("Actual nearest car park: " + el4)
print("Expected nearest car park: " + expected_nearest_parking)
if el4 == expected_nearest_parking:
        XLUtilities.writeData(path, "Info", 6, 4, "PASS")
        XLUtilities.writeData(path, "Info", 6, 2, el4)
else:
        XLUtilities.writeData(path, "Info", 6, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 6, 2, el4)

#5. Verify vehicle number:
expected_vehicle_number = XLUtilities.readData(path, "Info", 7, 3)
el5 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_vehicle_plate").get_attribute('text')
print("Actual vehicle number: " + el5)
print("Expected vehicle number: " + expected_vehicle_number)
if el5 == expected_vehicle_number:
        XLUtilities.writeData(path, "Info", 7, 4, "PASS")
        XLUtilities.writeData(path, "Info", 7, 2, el5)
else:
        XLUtilities.writeData(path, "Info", 7, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 7, 2, el5)

#click "NEXT STEP:PAYMENT" button
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/parking_primary_action_btn")):
        el6 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_primary_action_btn")
        el6.click()
except:
    pass


#Confirm detail page:

#9. Verifiy selected nearest car park:
expected_selected_nearest_parking = XLUtilities.readData(path, "Info", 9, 3)
el9 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.view.ViewGroup[1]/android.widget.LinearLayout/android.widget.TextView").get_attribute('text')
print("Actual selected nearest car park: " + el9)
print("Expected selected nearest car park: " + expected_selected_nearest_parking)
if el9 == expected_selected_nearest_parking:
        XLUtilities.writeData(path, "Info", 9, 4, "PASS")
        XLUtilities.writeData(path, "Info", 9, 2, el9)
else:
        XLUtilities.writeData(path, "Info", 9, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 9, 2, el9)

#10. Verify selected vehicle number:
expected_selected_vehicle_number = XLUtilities.readData(path, "Info", 10, 3)
el10 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.view.ViewGroup[2]/android.widget.TextView").get_attribute('text')
print("Actual selected vehicle number: " + el10)
print("Expected selected vehicle number: " + expected_selected_vehicle_number)
if el10 == expected_selected_vehicle_number:
        XLUtilities.writeData(path, "Info", 10, 4, "PASS")
        XLUtilities.writeData(path, "Info", 10, 2, el10)
else:
        XLUtilities.writeData(path, "Info", 10, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 10, 2, el10)

#11. Verifiy Credit card number:
expected_credit_card_number = XLUtilities.readData(path, "Info", 11, 3)
el11 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_card").get_attribute('text')
print("Actual credit card number: " + el11)
print("Expected credit card number: " + expected_credit_card_number)
if el11 == expected_credit_card_number:
        XLUtilities.writeData(path, "Info", 11, 4, "PASS")
        XLUtilities.writeData(path, "Info", 11, 2, el11)
else:
        XLUtilities.writeData(path, "Info", 11, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 11, 2, el11)

#12. selected parking duration:
expected_selected_parking_duration = XLUtilities.readData(path, "Info", 12, 3)
el12 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout/android.widget.TextView[1]").get_attribute('text')
print("Actual selected parking duration: " + el12)
print("Expected selected packing duration: " + str(expected_selected_parking_duration))
if el12 == str(expected_parking_duration):
        XLUtilities.writeData(path, "Info", 12, 4, "PASS")
        XLUtilities.writeData(path, "Info", 12, 2, el12)
else:
        XLUtilities.writeData(path, "Info", 12, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 12, 2, el12)

#13. estimated total cost:
expected_estimate_total_cost = XLUtilities.readData(path, "Info", 13, 3)
el13 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView").get_attribute('text')
print("Actual estimate total cost: " + el13)
print("Expected estimate total cost : " + str(expected_estimate_total_cost))
if el13 == str(expected_estimate_total_cost):
        XLUtilities.writeData(path, "Info", 13, 4, "PASS")
        XLUtilities.writeData(path, "Info", 13, 2, el13)
else:
        XLUtilities.writeData(path, "Info", 13, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 13, 2, el13)

#14 Click Start Parking button
Label_Start_Parking_Btn = XLUtilities.readData(path, "Info", 14, 3)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button")):
        Start_Parking_Btn = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button")
        el14 = Start_Parking_Btn.get_attribute('text')
        if el14 == Label_Start_Parking_Btn:
            XLUtilities.writeData(path, "Info", 14, 4, "PASS")
            XLUtilities.writeData(path, "Info", 14, 2, el14)
        else:
            XLUtilities.writeData(path, "Info", 14, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 14, 2, el14)
        Start_Parking_Btn.click()
except:
    pass

#Current Session page:

#16. Verifiy current session (address) :
expected_current_session_address = XLUtilities.readData(path, "Info", 16, 3)
el16 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_carpark").get_attribute('text')
print("Actual current session (address): " + el16)
print("Expected current session (address): " + expected_current_session_address)
if el16 == expected_current_session_address:
        XLUtilities.writeData(path, "Info", 16, 4, "PASS")
        XLUtilities.writeData(path, "Info", 16, 2, el16)
else:
        XLUtilities.writeData(path, "Info", 16, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 16, 2, el16)

#18. Verifiy current session (total cost) :
expected_current_session_cost = XLUtilities.readData(path, "Info", 18, 3)
el18 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_cost").get_attribute('text')
print("Actual current session (cost): " + el18)
print("Expected current session (cost): " + str(expected_current_session_cost))
if el18 == expected_current_session_cost:
        XLUtilities.writeData(path, "Info", 18, 4, "PASS")
        XLUtilities.writeData(path, "Info", 18, 2, el18)
else:
        XLUtilities.writeData(path, "Info", 18, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 18, 2, el18)

#19. Verifiy current session (vehicle) :
expected_current_session_vehicle = XLUtilities.readData(path, "Info", 19, 3)
el19 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_vehicle_plate").get_attribute('text')
print("Actual current session (vehicle): " + el19)
print("Expected current session (vehicle): " + expected_current_session_vehicle)
if el19 == expected_current_session_vehicle:
        XLUtilities.writeData(path, "Info", 19, 4, "PASS")
        XLUtilities.writeData(path, "Info", 19, 2, el19)
else:
        XLUtilities.writeData(path, "Info", 19, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 19, 2, el19)

#20. Click Extend button
label_Start_Parking_Btn = XLUtilities.readData(path, "Info", 20, 3)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/parking_secondary_action_btn")):
        Start_Parking_Btn = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_secondary_action_btn")
        el20 = Start_Parking_Btn.get_attribute('text')
        if el20 == label_Start_Parking_Btn:
            XLUtilities.writeData(path, "Info", 20, 4, "PASS")
            XLUtilities.writeData(path, "Info", 20, 2, el20)
        else:
            XLUtilities.writeData(path, "Info", 20, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 20, 2, el20)
        Start_Parking_Btn.click()
except:
    pass

#Extend session:

#22. Verify additional time:
expected_additional_time = XLUtilities.readData(path, "Info", 22, 3)
el22 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_duration_mins").get_attribute('text')
print("Actual additional time: " + el22)
print("Expected additional time: " + str(expected_additional_time))
if el22 == expected_additional_time:
        XLUtilities.writeData(path, "Info", 22, 4, "PASS")
        XLUtilities.writeData(path, "Info", 22, 2, el22)
else:
        XLUtilities.writeData(path, "Info", 22, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 22, 2, el22)

#23. Verify additional cost:
expected_additional_cost = XLUtilities.readData(path, "Info", 23, 3)
el23 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_cost").get_attribute('text')
print("Actual additional cost: " + el23)
print("Expected additional cost: " + str(expected_additional_cost))
if el23 == expected_additional_cost:
        XLUtilities.writeData(path, "Info", 23, 4, "PASS")
        XLUtilities.writeData(path, "Info", 23, 2, el23)
else:
        XLUtilities.writeData(path, "Info", 23, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 23, 2, el23)

#25. Click Extend parking button
label_Extend_Parking_Session_Btn =  XLUtilities.readData(path, "Info", 25, 3)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/primary_btn")):
        Extend_Parking_Session_Btn = driver.find_element_by_id("sg.gov.mnd.OneService:id/primary_btn")
        el25 = Extend_Parking_Session_Btn.get_attribute('text')
        if el25 == label_Extend_Parking_Session_Btn:
            XLUtilities.writeData(path, "Info", 25, 4, "PASS")
            XLUtilities.writeData(path, "Info", 25, 2, el25)
        else:
            XLUtilities.writeData(path, "Info", 25, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 25, 2, el25)
        Extend_Parking_Session_Btn.click()
except:
    pass


#24. Verify parking session extended message:
try:
    if WebDriverWait(driver, 3).until(lambda x: x.find_element_by_id("android:id/message")):
        expected_extend_message = XLUtilities.readData(path, "Info", 24, 3)
        el24 = driver.find_element_by_id("android:id/message").get_attribute('text')
        print("Actual parking session extended message: " + el24)
        print("Expected parking session extended message: " + str(expected_extend_message))
        if el24 == expected_extend_message:
                XLUtilities.writeData(path, "Info", 24, 4, "PASS")
                XLUtilities.writeData(path, "Info", 24, 2, el24)
        else:
                XLUtilities.writeData(path, "Info", 24, 4, "FAILED")
                XLUtilities.writeData(path, "Info", 24, 2, el24)
except:
    pass

#Click OK button on the messagebox
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("android:id/button1")):
        Msg_Extend_OK_Btn = driver.find_element_by_id("android:id/button1")
        Msg_Extend_OK_Btn.click()
except:
    pass


#26. Click End Early button
label_Msg_Extend_OK_Btn = XLUtilities.readData(path, "Info", 26, 3)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/parking_primary_action_btn")):
        Msg_Extend_OK_Btn = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_primary_action_btn")
        el26 = Msg_Extend_OK_Btn.get_attribute('text')
        if el26 == label_Msg_Extend_OK_Btn:
            XLUtilities.writeData(path, "Info", 26, 4, "PASS")
            XLUtilities.writeData(path, "Info", 26, 2, el26)
        else:
            XLUtilities.writeData(path, "Info", 26, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 26, 2, el26)
        Msg_Extend_OK_Btn.click()
except:
    pass

#27. Click End Now button
label_Msg_Extend_OK1_Btn = XLUtilities.readData(path, "Info", 27, 3)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/primary_btn")):
        Msg_Extend_OK1_Btn = driver.find_element_by_id("sg.gov.mnd.OneService:id/primary_btn")
        el27 = Msg_Extend_OK1_Btn.get_attribute('text')
        if el27 == label_Msg_Extend_OK1_Btn:
            XLUtilities.writeData(path, "Info", 27, 4, "PASS")
            XLUtilities.writeData(path, "Info", 27, 2, el27)
        else:
            XLUtilities.writeData(path, "Info", 27, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 27, 2, el27)
        Msg_Extend_OK1_Btn.click()
except:
    pass

#Transaction information:

#29. Verify duration:
expected_duration = XLUtilities.readData(path, "Info", 29, 3)
el29 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_duration_mins").get_attribute('text')
print("Actual duration (MIN): " + el29)
print("Expected duration (MIN): " + str(expected_duration))
if el29 == expected_duration:
        XLUtilities.writeData(path, "Info", 29, 4, "PASS")
        XLUtilities.writeData(path, "Info", 29, 2, el29)
else:
        XLUtilities.writeData(path, "Info", 29, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 29, 2, el29)

#30. Verify total cost:
expected_parking_cost = XLUtilities.readData(path, "Info", 30, 3)
el30 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_cost").get_attribute('text')
print("Actual parking cost: " + el30)
print("Expected parking cost: " + str(expected_parking_cost))
if el30 == expected_parking_cost:
        XLUtilities.writeData(path, "Info", 30, 4, "PASS")
        XLUtilities.writeData(path, "Info", 30, 2, el30)
else:
        XLUtilities.writeData(path, "Info", 30, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 30, 2, el30)

#31. Verify refund cost:
expected_parking_refund = XLUtilities.readData(path, "Info", 31, 3)
el31 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_refund").get_attribute('text')
print("Actual parking refund: " + el31)
print("Expected parking refund: " + str(expected_parking_refund))
if el31 == expected_parking_refund:
        XLUtilities.writeData(path, "Info", 31, 4, "PASS")
        XLUtilities.writeData(path, "Info", 31, 2, el31)
else:
        XLUtilities.writeData(path, "Info", 31, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 31, 2, el31)

#32. Verify vehicle number:
expected_parking_vehicle_number = XLUtilities.readData(path, "Info", 32, 3)
el32 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_vehicle_plate").get_attribute('text')
print("Actual parking refund: " + el32)
print("Expected parking refund: " + str(expected_parking_vehicle_number))
if el32 == expected_parking_vehicle_number:
        XLUtilities.writeData(path, "Info", 32, 4, "PASS")
        XLUtilities.writeData(path, "Info", 32, 2, el32)
else:
        XLUtilities.writeData(path, "Info", 32, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 32, 2, el32)

#33. Verify Address:
expected_parking_address = XLUtilities.readData(path, "Info", 33, 3)
el33 = driver.find_element_by_id("sg.gov.mnd.OneService:id/parking_carpark").get_attribute('text')
print("Actual parking address: " + el33)
print("Expected parking address: " + str(expected_parking_address))
if el33 == expected_parking_address:
        XLUtilities.writeData(path, "Info", 33, 4, "PASS")
        XLUtilities.writeData(path, "Info", 33, 2, el33)
else:
        XLUtilities.writeData(path, "Info", 33, 4, "FAILED")
        XLUtilities.writeData(path, "Info", 33, 2, el33)

# Click End Now button
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/dismiss_btn")):
        close_btn = driver.find_element_by_id("sg.gov.mnd.OneService:id/dismiss_btn")
        close_btn.click()
except:
    pass


#Navigate to user profile
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile").click()
except:
    pass

#Click logout button
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout").click()
except:
    pass

driver.quit()

