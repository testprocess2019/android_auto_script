import time
from appium import webdriver
def setUp():
    desired_caps = {
        'platformName': 'Android',
        'appPackage': 'sg.gov.mnd.OneService',
        'deviceName': '8LAE8LVCIBFAMNNF',
        #'deviceName': '10.100.2.149:5555',
        #'deviceId': '10.100.2.149:5555',
        'noReset': 'true',
        'app': 'C:\\Users\\PCCW Solutions\\Documents\\ApkProjects\\Android_OS4.2\\OneService_4.2.apk',
        'unicodekeyboard': 'true',
        'resetkeyboard': 'true'
    }
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)
    driver.implicitly_wait(20)
    return driver

if __name__ == '__main__':
    driver = setUp()