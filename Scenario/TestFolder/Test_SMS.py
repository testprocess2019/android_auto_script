import time
from appium import webdriver
import re

def setUpMessaging():
    desired_caps = {
        'platformName': 'Android',
        'appPackage': 'com.android.mms',
        'deviceName': '8LAE8LVCIBFAMNNF',
        'noReset': 'true',
        'app': 'C:\\Users\\PCCW Solutions\\Documents\\ApkProjects\\SMS_Apk\\Messaging_9.5.0.87.apk',
        'unicodekeyboard': 'true',
        'resetkeyboard': 'true'
    }
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)
    time.sleep(2)
    return driver

def RetrieveOTP(driver):
    driver.implicitly_wait(10)
    if driver.find_element_by_xpath("//android.widget.TextView[@text='OneService']").is_displayed():
        e1 = driver.find_element_by_xpath("//android.widget.TextView[@bounds='[252,541][885,601]']").get_attribute(
            'text')
        OTP = re.findall(r'\d+', e1)
        print(OTP)
        return OTP
    else:
        print("SMS OTP not found")
        pass


if __name__ == '__main__':
    driver = setUpMessaging()
    OTP = RetrieveOTP(driver)





