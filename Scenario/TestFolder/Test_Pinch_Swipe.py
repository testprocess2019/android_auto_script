# https://qxf2.com/blog/automating-pinch-zoom-swipe-appium/

"""
Qxf2: Example script to run test for a camera app using Appium
The test will cover some mobile gestures like tap, swipe, pinch and zoom.

"""
import unittest, time
from appium import webdriver
from time import sleep


class Android_Gestures(unittest.TestCase):
    "Class to run tests for mobile gestures"

    def setUp(self):
        "Setup for the test"
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '4.3'
        desired_caps['deviceName'] = 'GT-I9300'
        # Get the Package and Activity name to launch the Camera app
        desired_caps['appPackage'] = 'com.sec.android.app.camera'
        desired_caps['appActivity'] = 'com.sec.android.app.camera.Camera'
        # You may need to change the line below depending on your setup
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        "Tear down the test"
        self.driver.quit()

    def test_mobile_gestures(self):
        "Testing mobile gestures "
        self.driver.implicitly_wait(10)
        # Get the position of camera icon to take picture
        positions = [(1200, 360)]
        time.sleep(3)

        # BONUS: TAP
        # Perform Tap gesture on the position to take picture
        self.driver.tap(positions)

        # Find the android element of camera view
        elm = self.driver.find_element_by_id('com.sec.android.app.camera:id/GLSurfaceLayout')

        # 1. ZOOM
        # Perform zoom gesture- zoom(element=None, percent=200, steps=50)
        self.driver.zoom(element=elm)  # (doesnt work in android 4.2)
        # Tap to take close up snap
        self.driver.tap(positions)
        time.sleep(5)

        # 2. PINCH
        # Perform pinch gesture- pinch(element=None, percent=200, steps=50)
        self.driver.pinch(element=elm, percent=150, steps=100)  # (doesnt work in android 4.2)
        # Tap again to take zoomed out picture
        self.driver.tap(positions)
        time.sleep(5)

        # Press back button to go to home screen
        self.driver.keyevent(4)
        time.sleep(5)

        # 3. SWIPE
        # Perform swipe gesture on home screen- swipe(startX, startY, endX, endY, duration)
        self.driver.swipe(475, 500, 75, 500, 400)


# ---START OF SCRIPT
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Android_Gestures)
    unittest.TextTestRunner(verbosity=2).run(suite)