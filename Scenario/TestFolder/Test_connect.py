import unittest
from appium import webdriver

class Test_AndroidTests(unittest.TestCase):
    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['deviceName'] = '8LAE8LVCIBFAMNNF'
        desired_caps['app'] = 'C:\\Users\\PCCW Solutions\\AppData\\Local\\Android\\Sdk\\platform-tools\\sg.gov.mnd.OneService_411_apkplz.net.apk'
        desired_caps['appPackage'] = 'sg.gov.mnd.OneService'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Test_AndroidTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
