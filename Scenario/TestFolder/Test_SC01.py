
text = """ "Test12345" """
print(text)

print("Testing pass!!!")

'''
>>> print '"A word that needs quotation marks"'
"A word that needs quotation marks"
2) Escape the double quotes within the string:

>>> print "\"A word that needs quotation marks\""
"A word that needs quotation marks"
3) Use triple-quoted strings:

>>> print """ "A word that needs quotation marks" """
"A word that needs quotation marks"
'''

