import unittest
from appium import webdriver


class Test_Connection(unittest.TestCase):
    def setUp(self):
        desired_caps = {
            'platformName': 'Android',
            'appPackage': 'sg.gov.mnd.OneService',
            'deviceName': '8LAE8LVCIBFAMNNF',
            'noReset': 'true',
            'app': 'C:\\Users\\User\\AppData\\Local\\Android\\Sdk\\platform-tools\\OneService_411.apk'
        }
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)

    def test_label_Submit_case(self):
        self.driver.implicitly_wait(30)
        el1 = self.driver.find_element_by_id('sg.gov.mnd.OneService:id/submit_case_module_title').get_attribute('text')
        print(el1)
        self.assertEqual(el1, 'testing')

    def test_button_Submit_case(self):
        self.driver.implicitly_wait(30)
        el11 = self.driver.find_element_by_id('sg.gov.mnd.OneService:id/submit_case_module_icon')
        el11.is_displayed()
        el11.click()
        el12 = self.driver.find_element_by_id('sg.gov.mnd.OneService:id/dismiss_btn')
        el12.is_enabled()
        el12.click()


    def test_label2(self):
        self.driver.implicitly_wait(30)
        el2 = self.driver.find_element_by_id('sg.gov.mnd.OneService:id/eparking_module_title').get_attribute('text')
        el2 = el2.replace('\n', ' ')
        print(el2)
        self.assertEqual(el2, 'Start' + ' ' + 'Parking')

    def test_split_label3(self):
        self.driver.implicitly_wait(30)
        el3 = self.driver.find_element_by_id('sg.gov.mnd.OneService:id/find_parking_module_title').get_attribute('text')
        print(el3)
        self.assertEqual(el3.split(), ['Find', 'Parking'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            el3.split(2)

    def tearDown(self):
        self.driver.quit()




if __name__ == '__main__':
    unittest.main()
