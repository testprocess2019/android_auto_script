import requests
import json

def GetAPIConnect():
    url = 'http://www.weather.gov.sg/files/rss/rssHeavyRain_new.xml'
    #data = {"title": "<title>", "id": "<id>"}
    title = 'weather'
    apikey = 'wgs'
    params = {'t_apikey' : apikey, 't_title' : title}

    response = requests.get(url, params = params)
    r = response.json()
    return r

if __name__ == '__main__':
    APIinfo = GetAPIConnect()
    print(APIinfo)
    if APIinfo.get('Error'):
        message = APIinfo['Error']
    else:
        message = APIinfo['Plot']
    print(message)