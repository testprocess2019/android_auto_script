import Test_capability
import XLUtilities
from PIL import Image
from PIL import ImageChops
import time
from selenium.webdriver.support.ui import WebDriverWait
from appium.webdriver.common.touch_action import TouchAction

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\NearBy.xlsx"

#Click nearby icon
ele_nearby_btn = "sg.gov.mnd.OneService:id/bottom_navigation_nearby"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_nearby_btn)):
        driver.find_element_by_id(ele_nearby_btn).click()
except:
    pass

time.sleep(5)
#Click locate icon
ele_locate_btn = "sg.gov.mnd.OneService:id/locate_btn"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_locate_btn)):
        driver.find_element_by_id(ele_locate_btn).click()
except:
    pass

time.sleep(5)


time.sleep(5)
#Click case map icon
ele_case_map_btn = "sg.gov.mnd.OneService:id/case_map_button"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_case_map_btn)):
        driver.find_element_by_id(ele_case_map_btn).click()
except:
    pass

#Click case status dropdown list
ele_case_status_dropdown_list = "sg.gov.mnd.OneService:id/status_spinner"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_case_status_dropdown_list)):
        driver.find_element_by_id(ele_case_status_dropdown_list).click()
except:
    pass

#select status = Working On it
ele_select_status_Work_On_it = "//android.widget.TextView[@text='Working On It']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_select_status_Work_On_it)):
        driver.find_element_by_xpath(ele_select_status_Work_On_it).click()
except:
    pass

#Click case icon
ele_select_case_icon = "//android.widget.Image[@bounds='[342,422][392,492]']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_select_case_icon)):
        driver.find_element_by_xpath(ele_select_case_icon).click()
except:
    pass

#verify case detail information
ele_select_case_icon = "//android.widget.Image[@bounds='[342,422][392,492]']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_select_case_icon)):
        driver.find_element_by_xpath(ele_select_case_icon).click()
except:
    pass

'''
time.sleep(5)
#Click find parking icon
ele_find_parking_btn = "sg.gov.mnd.OneService:id/find_parking_button"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_find_parking_btn)):
        driver.find_element_by_id(ele_find_parking_btn).click()
except:
    pass

#Enter address to search cheapest car park
ele_search_address_field = "sg.gov.mnd.OneService:id/address_search_edit_text"
Exp_search_address = XLUtilities.readData(path, "Info", 3, 3)
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_search_address_field)):
        driver.find_element_by_id(ele_search_address_field).send_keys(Exp_search_address)
except:
    pass

#Enter address to search cheapest car park
ele_select_address_from_list = "sg.gov.mnd.OneService:id/list_content"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_select_address_from_list)):
        driver.find_element_by_id(ele_search_address_field).click()
except:
    pass

#select cheapest car park
ele_select_cheapest = "sg.gov.mnd.OneService:id/address_holder"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_select_cheapest)):
        driver.find_element_by_id(ele_select_cheapest).click()
except:
    pass
'''
driver.quit()