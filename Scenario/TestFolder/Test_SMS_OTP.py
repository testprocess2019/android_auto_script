import time
from appium import webdriver
import re

def setUpMessaging():
    desired_caps = {
        'platformName': 'Android',
        'appPackage': 'com.android.mms',
        'deviceName': '8LAE8LVCIBFAMNNF',
        'noReset': 'true',
        'app': 'C:\\Users\\PCCW Solutions\\Documents\\ApkProjects\\SMS_Apk\\Messaging_9.5.0.87.apk',
        'unicodekeyboard': 'true',
        'resetkeyboard': 'true'
    }
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)
    driver.implicitly_wait(10)
    if driver.find_element_by_xpath("//android.widget.TextView[@text='OneService']").is_displayed():
        e1 = driver.find_element_by_xpath("//android.widget.TextView[@bounds='[252,541][885,601]']").get_attribute(
            'text')
        OTP = re.findall(r'\d+', e1)
        print(OTP)
        return OTP
    else:
        print("SMS OTP not found")
        pass

def backPrevious(OTP):
    desired_caps = {
        'platformName': 'Android',
        'appPackage': 'sg.gov.mnd.OneService',
        'deviceName': '8LAE8LVCIBFAMNNF',
        'noReset': 'true',
        'app': 'C:\\Users\\PCCW Solutions\\Documents\\ApkProjects\sg.gov.mnd.OneService_411_apkplz.net\\OneService_411.apk',
        'unicodekeyboard': 'true',
        'resetkeyboard': 'true'
    }
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)
    driver.start_activity("sg.gov.mnd.OneService","sg.gov.mnd.OneService.UserProfile.VerifyPageActivity")
    print("Current activity:", driver.current_package)
    el1 = driver.find_element_by_id("sg.gov.mnd.OneService:id/pin_view")
    el1.send_keys(OTP)

if __name__ == '__main__':
    OTP = setUpMessaging()


