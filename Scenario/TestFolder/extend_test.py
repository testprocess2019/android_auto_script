# coding=utf-8
import unittest
import os
from Test_img import Appium_Extend
from appium import webdriver
import Test_capability
driver = Test_capability.setUp()

class Test(unittest.TestCase):
    # 初始化环境
    def setUp(self):
        self.extend = Appium_Extend(self.driver)
        # 回到主屏幕
        self.driver.press_keycode(3)

    # 退出测试
    def tearDown(self):
        self.driver.quit()

    def test_get_screen_by_element(self):
        element = self.driver.find_element_by_id("sg.gov.mnd.OneService:id/mso_logo")

        self.extend.get_screenshot_by_element(element).write_to_file("C:\\Users\\PCCW Solutions\\Documents\\Test Image", "image")
        self.assertTrue(os.path.isfile("C:\\Users\\PCCW Solutions\\Documents\\Test Image\\image.png"))

    def test_same_as(self):
        element = self.driver.find_element_by_id("sg.gov.mnd.OneService:id/mso_logo")

        load = self.extend.load_image("C:\\Users\\PCCW Solutions\\Documents\\Test Image\\image.png")
        # 要求百分百相似
        result = self.extend.get_screenshot_by_element(element).same_as(load, 0)
        self.assertTrue(result)


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(Test("test_get_screen_by_element"))
    suite.addTest(Test("test_same_as"))
    # 执行测试
    unittest.TextTestRunner().run(suite)
