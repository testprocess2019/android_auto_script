import csv
import unittest
from csv import DictWriter


# This is the csv file read/write class.
class CSVFileOperator(object):
    line_return_character = '\r\n'
    comma_separator = ','

    # Write a data_list into a csv file identified by file_path.
    # Each element in the data_list is also a list object represent one row.    
    def write_list_to_csv_file(self, data_list, file_path):

        try:
            file_object = open(file_path, 'w')
            csv_file_writer = csv.writer(file_object, delimiter=",", quoting=csv.QUOTE_ALL, quotechar="'")

            for item in data_list:
                csv_file_writer.writerow(item)

            print(file_path + " has been created successfully.")
        except FileNotFoundError:
            print(file_path + " not found.")

    # Write a data_list into a csv file identified by file_path.
    # Each element in the data_list is a dictionary object represent one row.    
    def write_dict_to_csv_file(self, data_list, column_name_list, file_path):
        try:
            file_object = open(file_path, 'w')
            csv_dict_file_writer = csv.DictWriter(file_object, fieldnames=column_name_list)
            csv_dict_file_writer.writeheader()

            for item in data_list:
                csv_dict_file_writer.writerow(item)

            print(file_path + " has been created successfully.")
        except FileNotFoundError:
            print(file_path + " not found.")

    # Read csv file line by line and return the content.
    def read_csv_file_by_reader(self, file_path):
        ret = ''
        try:
            file_object = open(file_path, 'r')
            csv_file_reader = csv.reader(file_object, delimiter=',')
            row_number = 1

            for row in csv_file_reader:
                # Get one row list data string value,
                # below code can avoid type convert error if column has number or boolean value.
                row_str = ''
                row_size = len(row)
                for i in range(row_size):
                    row_str += str(row[i]) + self.comma_separator

                print("row_" + str(row_number) + " = " + row_str)
                ret += row_str
                ret += self.line_return_character
                row_number += 1
        except FileNotFoundError:
            print(file_path + " not found.")
        finally:
            print("ret = " + ret)
            return ret

            # Read csv file line by line and return the content.

    def read_csv_file_by_dict_reader(self, file_path):
        ret = ''
        try:
            file_object = open(file_path, 'r')
            # csv.DictReader will return a dictionary list.
            csv_file_dict_reader = csv.DictReader(file_object)

            # Get csv file fields name list.
            field_names = csv_file_dict_reader.fieldnames

            # Get field name list size.
            field_name_size = len(field_names)

            # Get each field name.
            for i in range(field_name_size):
                field_name = field_names[i]
                ret += field_name + self.comma_separator

            # Add line return character.
            ret += self.line_return_character
            print("Field Names : " + ret)

            row_number = 1
            # Each row is one dictionary.
            for row in csv_file_dict_reader:
                row_str = ''
                # Loop the row field name.
                for i in range(field_name_size):
                    field_name = field_names[i]
                    # Get field value in this row. Convert to string to avoid type convert error.
                    row_str += str(row[field_name]) + self.comma_separator

                print("row_" + str(row_number) + " = " + row_str)
                ret += row_str
                ret += self.line_return_character
                row_number += 1
        except FileNotFoundError:
            print(file_path + " not found.")
        finally:
            print("ret = " + ret)
            return ret

        # This is the test case class for CSVFileOperator class.


class TestCSVFileOperator(unittest.TestCase):

    # Test CSVFileOperator's write_list_to_csv_file.
    def test_write_list_to_csv_file(self):
        data_row_header = ['Name', 'Email', 'Title']
        data_row_1 = ['jerry', 'jerry@dev2qa.com', 'CEO']
        data_row_2 = ['tom', 'tom@dev2qa.com', 'Developer']
        data_list = [data_row_header, data_row_1, data_row_2]
        csv_file_operator = CSVFileOperator()
        csv_file_operator.write_list_to_csv_file(data_list, "./csv_user_info.csv")

        # Test CSVFileOperator's write_dict_to_csv_file.

    def test_write_dict_to_csv_file(self):
        # First create some dictionary type data.
        data_row_header = ['Coding Language', 'Popularity']
        data_row_1 = {'Coding Language': 'Java', 'Popularity': '17797'}
        data_row_2 = {'Coding Language': 'JavaScript', 'Popularity': '18998'}
        data_row_3 = {'Coding Language': 'Python', 'Popularity': '29898'}
        data_row_4 = {'Coding Language': 'C++', 'Popularity': '16567'}
        data_row_5 = {'Coding Language': 'C', 'Popularity': '9989'}
        data_row_6 = {'Coding Language': 'Html', 'Popularity': '18983'}

        # Add dictionary data into a list
        data_list = [data_row_1, data_row_2, data_row_3, data_row_4, data_row_5, data_row_6]
        csv_file_operator = CSVFileOperator()
        csv_file_operator.write_dict_to_csv_file(data_list, data_row_header, "./csv_coding_language.csv")

    def test_read_csv_file_by_reader(self):
        csv_file_operator = CSVFileOperator()
        csv_file_operator.read_csv_file_by_reader("C:/Users/User/Documents/CarParkInfo.csv")

    def test_read_csv_file_by_dict_reader(self):
        csv_file_operator = CSVFileOperator()
        csv_file_operator.read_csv_file_by_dict_reader("C:/Users/User/Documents/CarParkInfo.csv")


def run_all_test_case():
    unittest.main()


def run_special_test_case(test):
    # Create a TestSuite object.
    test_suite = unittest.TestSuite()
    # Add test.
    test_suite.addTest(test)
    # Create a TestResult object to save test result info.
    test_result = unittest.TestResult()
    # Run the test suite.
    test_suite.run(test_result)


if __name__ == '__main__':
    run_all_test_case()
    run_special_test_case(TestCSVFileOperator("test_read_csv_file_by_reader"))
    run_special_test_case(TestCSVFileOperator("test_read_csv_file_by_dict_reader"))
    run_special_test_case(TestCSVFileOperator("test_write_list_to_csv_file"))
    run_special_test_case(TestCSVFileOperator("test_write_dict_to_csv_file"))