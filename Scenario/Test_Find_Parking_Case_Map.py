import Test_capability
import time
import XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Find_Park_Info.xlsx"
path2 = "C:\\logs\\Artefacts.png"

time.sleep(30)

# Launch find parking page:
# Click find parking button
ele_Find_parking_Btn = "sg.gov.mnd.OneService:id/find_parking_module_icon"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_Find_parking_Btn)):
        driver.find_element_by_id(ele_Find_parking_Btn).click()
except:
    pass

#Close tooltip
ele_tips_holder = "sg.gov.mnd.OneService:id/tip_sort_by"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_tips_holder)):
        driver.find_element_by_id(ele_tips_holder).click()
except:
    pass

time.sleep(10)
# Click location icon
ele_locate_icon = "sg.gov.mnd.OneService:id/locate_btn"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_locate_icon)):
        driver.find_element_by_id(ele_locate_icon).click()
except:
    pass

# Click case map icon
ele_case_map_icon = "sg.gov.mnd.OneService:id/case_map_button"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_case_map_icon)):
        driver.find_element_by_id(ele_case_map_icon).click()
except:
    pass

time.sleep(10)
# Click on all catergory dropdown list
ele_all_cat_dropdown = "sg.gov.mnd.OneService:id/category_spinner"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_all_cat_dropdown)):
        driver.find_element_by_id(ele_all_cat_dropdown).click()
except:
    pass

time.sleep(10)
# select a category from dropdownlist
ele_test_3 = "//android.widget.TextView[@text='Cleanliness']"
ele_cat_from_excel = XLUtilities.readData(path, "Info", 3, 4)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_test_3)):
        el_3 = driver.find_element_by_xpath(ele_test_3).get_attribute('text')
        driver.find_element_by_xpath(ele_test_3).click()
        print('Selected catergory: ' + el_3)
        if el_3 == ele_cat_from_excel:
            XLUtilities.writeData(path, "Info", 3, 6, "PASS")
            XLUtilities.writeData(path, "Info", 3, 2, el_3)
        else:
            XLUtilities.writeData(path, "Info", 3, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 3, 2, el_3)
except:
    pass

time.sleep(10)
# Click on all status dropdown list
ele_all_status_dropdown = "sg.gov.mnd.OneService:id/status_spinner"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_all_status_dropdown)):
        driver.find_element_by_id(ele_all_status_dropdown).click()
except:
    pass

time.sleep(10)
# select case status from dropdownlist
ele_test_4 = "//android.widget.TextView[@text='Working On It']"
ele_status_from_excel = XLUtilities.readData(path, "Info", 4, 4)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_test_4)):
        el_4 = driver.find_element_by_xpath(ele_test_4).get_attribute('text')
        driver.find_element_by_xpath(ele_test_4).click()
        print('Selected status: ' + el_4)
        if el_4 == ele_status_from_excel:
            XLUtilities.writeData(path, "Info", 4, 6, "PASS")
            XLUtilities.writeData(path, "Info", 4, 2, el_4)
        else:
            XLUtilities.writeData(path, "Info", 4, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 4, 2, el_4)
except:
    pass
'''
# Click location icon
ele_locate_icon2 = "sg.gov.mnd.OneService:id/locate_btn"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_locate_icon2)):
        driver.find_element_by_id(ele_locate_icon2).click()
except:
    pass
'''
driver.get_screenshot_as_file(path2)
loc1 = "H3"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot attached...")

time.sleep(5)

XLUtilities.close(path)
driver.quit()
