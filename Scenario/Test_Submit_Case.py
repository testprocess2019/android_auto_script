import Test_capability
import XLUtilities
from selenium import webdriver
from PIL import ImageChops, Image
import time
from selenium.webdriver.support.wait import WebDriverWait

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Submit_Case.xlsx"

time.sleep(30)

# Screenshot file path
path1 = 'C:\\logs\\ActFullSubCat1.png'
path2 = 'C:\\logs\\ActFullSubCat2.png'
path3 = 'C:\\logs\\ExpFullSubCat1.png'
path4 = 'C:\\logs\\ExpFullSubCat2.png'

# Click submit case icon
ele_submit_case_module_icon = "sg.gov.mnd.OneService:id/submit_case_module_icon"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_submit_case_module_icon)):
        driver.find_element_by_id(ele_submit_case_module_icon).click()
except:
    pass

# Get Category list element size
ele_case_submission_category_list = "sg.gov.mnd.OneService:id/case_submission_category_list"
global Cat_Image
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_case_submission_category_list)):
        Cat_Image = driver.find_element_by_id(ele_case_submission_category_list)
except:
    pass
#Cat_Image = driver.find_element_by_id(ele_case_submission_category_list)

# Screenshot Cat1 and Cat2
time.sleep(10)
driver.get_screenshot_as_file(path1)
ele_cat1 = "//android.widget.TextView[@text='Drains & Sewers']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_cat1)):
        driver.find_element_by_xpath(ele_cat1).click()
except:
    pass

time.sleep(10)
driver.get_screenshot_as_file(path2)

#Open Image file base on file path
Act_Image_Sub_Cat1 = Image.open(path1)
Act_Image_Sub_Cat2 = Image.open(path2)
Exp_Image_Sub_Cat1 = Image.open(path3)
Exp_Image_Sub_Cat2 = Image.open(path4)

Cat_Image_Loc = Cat_Image.location
Cat_size = Cat_Image.size
Image_box = (Cat_Image_Loc["x"], Cat_Image_Loc["y"], Cat_Image_Loc["x"] + Cat_size["width"], Cat_Image_Loc["y"] + Cat_size["height"])

Act_Image_Sub_Cat1_Crop = Act_Image_Sub_Cat1.crop(Image_box)
Act_Image_Sub_Cat1_Crop.save(path1)
Act_Image_Sub_Cat2_Crop = Act_Image_Sub_Cat2.crop(Image_box)
Act_Image_Sub_Cat2_Crop.save(path2)

Act_Image_Sub_Cat1_Final = Image.open(path1)
Act_Image_Sub_Cat2_Final = Image.open(path2)

#Compare Image file
diff_Cat1 = ImageChops.difference(Act_Image_Sub_Cat1_Final, Exp_Image_Sub_Cat1)
diff_Cat2 = ImageChops.difference(Act_Image_Sub_Cat2_Final, Exp_Image_Sub_Cat2)


if diff_Cat1.getbbox():
    Logo_Test_Result = "Cat1 images are different"
    XLUtilities.writeData(path, "Info", 3, 4, "FAILED")
    loc1 = "B3"
    XLUtilities.InsertImg(path, loc1, path1)
    print(Logo_Test_Result)
else:
    Logo_Test_Result = "Cat1 images are same"
    XLUtilities.writeData(path, "Info", 3, 4, "PASS")
    loc1 = "B3"
    XLUtilities.InsertImg(path, loc1, path1)
    print(Logo_Test_Result)

if diff_Cat2.getbbox():
    Logo_Test_Result = "Cat2 images are different"
    XLUtilities.writeData(path, "Info", 4, 4, "FAILED")
    loc2 = "B4"
    XLUtilities.InsertImg(path, loc2, path2)
    print(Logo_Test_Result)
else:
    Logo_Test_Result = "Cat2 images are same"
    XLUtilities.writeData(path, "Info", 4, 4, "PASS")
    loc2 = "B4"
    XLUtilities.InsertImg(path, loc2, path2)
    print(Logo_Test_Result)


driver.implicitly_wait(10)
driver.swipe(12, 538, 702, 540, 405)
time.sleep(5)

#Click Cleanliness icon
ele_Cat1_Cls = "//android.widget.TextView[@text='Cleanliness']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_Cat1_Cls)):
        driver.find_element_by_xpath(ele_Cat1_Cls).click()
except:
    pass

driver.implicitly_wait(10)
driver.swipe(336, 1161, 340, 769, 400)
time.sleep(5)

#Click Sub-Cat Dirty public area icon
ele_Cat1_Sub_DPA = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_Cat1_Sub_DPA)):
        driver.find_element_by_xpath(ele_Cat1_Sub_DPA).click()
except:
    pass

driver.implicitly_wait(10)
#Click Add Photo icon
ele_Cat1_Sub_DPA_Add_Photo = "sg.gov.mnd.OneService:id/add_photo"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_Cat1_Sub_DPA_Add_Photo)):
        driver.find_element_by_id(ele_Cat1_Sub_DPA_Add_Photo).click()
except:
    pass

#Allow Oneservice to access photo
ele_Allow_Access_photo_Btn = "com.android.packageinstaller:id/permission_allow_button"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_Allow_Access_photo_Btn)):
        driver.find_element_by_id(ele_Allow_Access_photo_Btn).click()
except:
    pass

#Allow Oneservice to take photo
ele_Allow_take_photo_Btn = "com.android.packageinstaller:id/permission_allow_button"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_Allow_take_photo_Btn)):
        driver.find_element_by_id(ele_Allow_take_photo_Btn).click()
except:
    pass

#Click Add Photo icon from gallery
ele_Cat1_Sub_DPA_Add_Photo_Gly = "sg.gov.mnd.OneService:id/camera_swipe_btn"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_Cat1_Sub_DPA_Add_Photo_Gly)):
        driver.find_element_by_id(ele_Cat1_Sub_DPA_Add_Photo_Gly).click()
except:
    pass

#Click Add Photo icon from gallery choose a picture
ele_Cat1_Sub_DPA_Add_Photo_Gly_1 = "//android.widget.ImageView[@bounds='[4,207][239,447]']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_Cat1_Sub_DPA_Add_Photo_Gly_1)):
        driver.find_element_by_xpath(ele_Cat1_Sub_DPA_Add_Photo_Gly_1).click()
except:
    pass

#Click Next button
ele_Next_Btn = "sg.gov.mnd.OneService:id/action_btn_in_footer"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_Next_Btn)):
        driver.find_element_by_id(ele_Next_Btn).click()
except:
    pass

#Click Finish button
ele_Finish_Btn = "//android.widget.TextView[@text='Finish']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_Finish_Btn)):
        driver.find_element_by_xpath(ele_Finish_Btn).click()
except:
    pass

#Provide detail of the issue
ele_enter_detail_issue = "sg.gov.mnd.OneService:id/case_submission_description"
Test_value = 'Test test 1234 1234 !@#$%'
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_enter_detail_issue)):
        driver.find_element_by_id(ele_enter_detail_issue).send_keys(Test_value)
except:
    pass

#Hide the keyboard after enter detail issue
driver.hide_keyboard()

time.sleep(5)
#select report date
ele_sub_datetime = "sg.gov.mnd.OneService:id/case_submission_datetime"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_sub_datetime)):
        driver.find_element_by_id(ele_sub_datetime).click()
except:
    pass

#select report date with done button
ele_sub_datetime_ok = "android:id/button1"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_sub_datetime_ok)):
        driver.find_element_by_id(ele_sub_datetime_ok).click()
except:
    pass
'''
time.sleep(5)
#Swipe the screen to see the "select location"
driver.swipe(288, 1300, 336, 836, 400)
'''

#click select location
ele_select_issue_loc = "sg.gov.mnd.OneService:id/case_submission_address"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_select_issue_loc)):
        driver.find_element_by_id(ele_select_issue_loc).click()
except:
    pass

time.sleep(5)
#click locator
ele_locator = "sg.gov.mnd.OneService:id/locate_btn"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_locator)):
        locator_btn = driver.find_element_by_id(ele_locator).click()
except:
    pass
'''
# Screenshot Address
ele_Act_Address_search_info = "sg.gov.mnd.OneService:id/search_bar"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_Act_Address_search_info)):
        Act_Address_search_info = driver.find_element_by_id(ele_Act_Address_search_info).get_attribute('text')
except:
    pass
'''

#14 Click Start Parking button
Exp_Address_search_info = XLUtilities.readData(path, "Info", 6, 3)
ele_Start_Parking_Btn = "sg.gov.mnd.OneService:id/search_bar"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_Start_Parking_Btn)):
        Act_Address_search_info = driver.find_element_by_id(ele_Start_Parking_Btn).get_attribute('text')
        if Act_Address_search_info == Exp_Address_search_info:
            XLUtilities.writeData(path, "Info", 6, 4, "PASS")
            XLUtilities.writeData(path, "Info", 6, 2, Act_Address_search_info)
        else:
            XLUtilities.writeData(path, "Info", 6, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 6, 2, Act_Address_search_info)
except:
    pass

time.sleep(10)
#Get Address element size
ele_address_ele_size = "sg.gov.mnd.OneService:id/content"
global Address_Image
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_address_ele_size)):
        Address_Image = driver.find_element_by_id(ele_address_ele_size)
except:
    pass
#Address_Image = driver.find_element_by_id(ele_address_ele_size)
path5 = "C:\\logs\\ActAddressScreen.png"
path6 = "C:\\logs\\ExpAddressScreen.png"

driver.get_screenshot_as_file(path5)
Act_Image_Address_1 = Image.open(path5)

Address_Image_Loc = Address_Image.location
Address_size = Address_Image.size
Address_Image_box = (Address_Image_Loc["x"], Address_Image_Loc["y"], Address_Image_Loc["x"] + Address_size["width"], Address_Image_Loc["y"] + Address_size["height"])

Act_Image_Address_Crop = Act_Image_Address_1.crop(Address_Image_box)
Act_Image_Address_Crop.save(path5)

Act_Image_Address_Final = Image.open(path5)
Exp_Image_Address_Final = Image.open(path6)

#Compare Image file
diff_Address_1 = ImageChops.difference(Act_Image_Address_Final, Exp_Image_Address_Final)

if diff_Address_1.getbbox():
    Address_Test_Result = "Address images are different"
    XLUtilities.writeData(path, "Info", 5, 4, "FAILED")
    loc1 = "B5"
    XLUtilities.InsertImg(path, loc1, path5)
    print(Address_Test_Result)
else:
    Address_Test_Result = "Address images are same"
    XLUtilities.writeData(path, "Info", 5, 4, "PASS")
    loc1 = "B5"
    XLUtilities.InsertImg(path, loc1, path5)
    print(Address_Test_Result)

#Click confirm address button
ele_click_confirm_address_btn = "sg.gov.mnd.OneService:id/button_confirm_address"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_click_confirm_address_btn)):
        driver.find_element_by_id(ele_click_confirm_address_btn).click()
except:
    pass

time.sleep(5)

XLUtilities.close(path)
driver.quit()
