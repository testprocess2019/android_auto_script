import Test_capability
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_capability.setUp()

try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/login_tab_sp_btn")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/login_tab_sp_btn").click()
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile").click()
except:
    pass


try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout").click()
except:
    pass