import Test_capability
import time
import XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

google_account = "//android.widget.TextView[@text='mso.osappinbox@gmail.com']"
driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\User_Login.xlsx"
path2 = "C:\\logs\\Artefacts.png"

ele_login_tab_gg_btn = "sg.gov.mnd.OneService:id/login_tab_gg_btn"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_login_tab_gg_btn)):
        driver.find_element_by_id(ele_login_tab_gg_btn).click()
except:
    pass

try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(google_account)):
        driver.find_element_by_xpath(google_account).click()
except:
    pass

time.sleep(10)

ele_bottom_navigation_profile = "sg.gov.mnd.OneService:id/bottom_navigation_profile"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_bottom_navigation_profile)):
        driver.find_element_by_id(ele_bottom_navigation_profile).click()
except:
    pass

Username = XLUtilities.readData(path, "Info", 7, 3)
ele_user_profile_userid = "sg.gov.mnd.OneService:id/user_profile_userid"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_user_profile_userid)):
        actual_username = driver.find_element_by_id(ele_user_profile_userid).get_attribute("text")
        print("Actual username: " + actual_username)
        print("Expected username: " + Username)
        if actual_username == Username:
            XLUtilities.writeData(path, "Info", 7, 4, "PASS")
            XLUtilities.writeData(path, "Info", 7, 2, actual_username)
        else:
            XLUtilities.writeData(path, "Info", 7, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 7, 2, actual_username)
except:
    pass

mobileNum = XLUtilities.readData(path, "Info", 8, 3)
ele_mobileNum = "sg.gov.mnd.OneService:id/user_profile_mobileno"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_mobileNum)):
        actual_mobileNo = driver.find_element_by_id(ele_mobileNum).get_attribute("text")
        print("Actual username: " + actual_mobileNo)
        print("Expected username: " + str(mobileNum))
        if actual_mobileNo == str(mobileNum):
            XLUtilities.writeData(path, "Info", 8, 4, "PASS")
            XLUtilities.writeData(path, "Info", 8, 2, actual_mobileNo)
        else:
            XLUtilities.writeData(path, "Info", 8, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 8, 2, actual_mobileNo)
except:
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B9"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot attached...")

time.sleep(5)
XLUtilities.close(path)
driver.quit()