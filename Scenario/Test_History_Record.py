import time
import Test_capability
import XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\History_Info.xlsx"
path2 = "C:\\logs\\ActTrxHistory.png"

time.sleep(40)

Exp_history_report_item = XLUtilities.readData(path, "Info", 3, 3)
ele_history_report_item = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView[2]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_history_report_item)):
        el_3 = driver.find_element_by_xpath(ele_history_report_item).get_attribute('text')
        print('History report item: ' + el_3)
        if el_3 != '':
            XLUtilities.writeData(path, "Info", 3, 4, "PASS")
            XLUtilities.writeData(path, "Info", 3, 2, el_3)
        else:
            XLUtilities.writeData(path, "Info", 3, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 3, 2, el_3)
except:
    XLUtilities.writeData(path, "Info", 3, 4, "FAILED")
    pass

driver.swipe(676, 1213, 675, 815, 400)
time.sleep(5)

Exp_history_transact_item = XLUtilities.readData(path, "Info", 4, 3)
ele_history_transact_item = "//android.view.ViewGroup[2]/android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView[4]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_history_transact_item)):
        el_4 = driver.find_element_by_xpath(ele_history_transact_item).get_attribute('text')
        print('History transact item: ' + el_4)
        if el_4 != '':
            XLUtilities.writeData(path, "Info", 4, 4, "PASS")
            XLUtilities.writeData(path, "Info", 4, 2, el_4)
        else:
            XLUtilities.writeData(path, "Info", 4, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 4, 2, el_4)
except:
    XLUtilities.writeData(path, "Info", 4, 4, "FAILED")
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B5"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot for report & transact attached...")

#Click history button
ele_History_Btn = "sg.gov.mnd.OneService:id/bottom_navigation_history"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_History_Btn)):
        driver.find_element_by_id(ele_History_Btn).click()
except:
    pass

#Close tooltip
ele_tips_holder1 = "sg.gov.mnd.OneService:id/tip_msg_3"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_tips_holder1)):
        driver.find_element_by_id(ele_tips_holder1).click()
except:
    pass

#Click Cases button
ele_cases_Btn = "//android.widget.TextView[@text='Cases']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_cases_Btn)):
        Act_Address_search_info = driver.find_element_by_xpath(ele_cases_Btn).click()
except:
    pass

# Verify reported case should be displayed
Exp_history_cases_item = XLUtilities.readData(path, "Info", 6, 3)
ele_history_cases_item = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView"
Act_1st_case_title = XLUtilities.readData(path, "Info", 3, 2)

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_history_cases_item)):
        el_6 = driver.find_element_by_xpath(ele_history_cases_item).get_attribute('text')
        print('History transact item: ' + el_6)
        if el_6 == Act_1st_case_title:
            XLUtilities.writeData(path, "Info", 6, 4, "PASS")
            XLUtilities.writeData(path, "Info", 6, 2, el_6)
        else:
            XLUtilities.writeData(path, "Info", 6, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 6, 2, el_6)
except:
    XLUtilities.writeData(path, "Info", 6, 4, "FAILED")
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B7"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot for reported case attached...")

#Click Digital parking button
ele_Digital_Parking_Btn = "//android.widget.TextView[@text='Digital Parking']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_Digital_Parking_Btn)):
        Act_Address_search_info = driver.find_element_by_xpath(ele_Digital_Parking_Btn).click()
except:
    pass

#Close tooltip
ele_tips_holder2 = "sg.gov.mnd.OneService:id/tip_msg_3"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_tips_holder2)):
        driver.find_element_by_id(ele_tips_holder2).click()
except:
    pass

driver.swipe(407, 400, 396, 1013, 400)
time.sleep(10)

# Verify reported case should be displayed
ele_history_digital_parking_record = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[1]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_history_digital_parking_record)):
        el_8 = driver.find_element_by_xpath(ele_history_digital_parking_record).get_attribute('text')
        print('Session records start date: ' + el_8)
        if el_8 != '':
            XLUtilities.writeData(path, "Info", 8, 4, "PASS")
            XLUtilities.writeData(path, "Info", 8, 2, el_8)
        else:
            XLUtilities.writeData(path, "Info", 8, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 8, 2, el_8)
except:
    XLUtilities.writeData(path, "Info", 8, 4, "FAILED")
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B9"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot digital parking attached...")


#Click Digital parking bills button
ele_Digital_Parking_BILLS_Btn = "//android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.TextView[@text='BILLS']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_Digital_Parking_BILLS_Btn)):
        driver.find_element_by_xpath(ele_Digital_Parking_BILLS_Btn).click()
except:
    XLUtilities.writeData(path, "Info", 10, 4, "FAILED")
    pass

# Verify bill record should be displayed
ele_history_digital_parking_bill_record = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup/android.widget.TextView[1]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_history_digital_parking_bill_record)):
        el_10 = driver.find_element_by_xpath(ele_history_digital_parking_bill_record).get_attribute('text')
        print('Bill records start date: ' + el_10)
        if el_10 != '':
            XLUtilities.writeData(path, "Info", 10, 4, "PASS")
            XLUtilities.writeData(path, "Info", 10, 2, el_10)
        else:
            XLUtilities.writeData(path, "Info", 10, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 10, 2, el_10)
except:
    XLUtilities.writeData(path, "Info", 10, 4, "FAILED")
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B11"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot digital parking-bill attached...")


#Click Digital parking button
ele_EPS_Parking_Btn = "//android.widget.TextView[@text='EPS Parking']"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(ele_EPS_Parking_Btn)):
        ele_EPS_Parking_Btn = driver.find_element_by_xpath(ele_EPS_Parking_Btn).click()
except:
    pass

#Close tooltip
ele_tips_holder3 = "sg.gov.mnd.OneService:id/tip_msg_3"
try:
    if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(ele_tips_holder3)):
        driver.find_element_by_id(ele_tips_holder3).click()
except:
    pass

#Close selective records shown
ele_selective_records_shown_OK_Btn = "android:id/button2"
try:
    if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(ele_selective_records_shown_OK_Btn)):
        driver.find_element_by_id(ele_selective_records_shown_OK_Btn).click()
except:
    pass

driver.swipe(407, 400, 396, 1013, 400)
time.sleep(10)

# Verify EPS parking record should be displayed
ele_history_EPS_Parking_record = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup[3]/android.widget.TextView[1]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_history_EPS_Parking_record)):
        el_12 = driver.find_element_by_xpath(ele_history_EPS_Parking_record).get_attribute('text')
        print('EPS parking records start date: ' + el_12)
        if el_12 != '':
            XLUtilities.writeData(path, "Info", 12, 4, "PASS")
            XLUtilities.writeData(path, "Info", 12, 2, el_12)
        else:
            XLUtilities.writeData(path, "Info", 12, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 12, 2, el_12)
except:
    XLUtilities.writeData(path, "Info", 12, 4, "FAILED")
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B13"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot EPS parking attached...")

XLUtilities.close(path)
driver.quit()