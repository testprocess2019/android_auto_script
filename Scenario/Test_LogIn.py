import time
import Test_capability
import XLUtilities
from selenium.webdriver.support.ui import WebDriverWait
import re

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\User_Login.xlsx"
path2 = "C:\\logs\\Artefacts.png"
#rows = XLUtilities.getRowCount(path, "Info")
Username = XLUtilities.readData(path, "Info", 3, 3)
mobileNum = XLUtilities.readData(path, "Info", 4, 3)
print(Username)
print(mobileNum)

#Enter Username
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/login_tab_userid")):
        el1 = driver.find_element_by_id("sg.gov.mnd.OneService:id/login_tab_userid")
        el1.clear()
        el1.send_keys(Username)
except:
    pass

#Enter mobile number
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/login_tab_mobileno")):
        el2 = driver.find_element_by_id("sg.gov.mnd.OneService:id/login_tab_mobileno")
        el2.clear()
        el2.send_keys(mobileNum)
except:
    pass

#Click log in button
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/login_action_btn")):
        el3 = driver.find_element_by_id("sg.gov.mnd.OneService:id/login_action_btn")
        if el3.is_enabled():
            el3.click()
        else:
            pass
except:
    pass

#Waiting for SMS OTP
time.sleep(10)
driver.open_notifications()

#open notification and retrieve the OTP
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath("//android.widget.TextView[@text='OneService']")):
        e1 = driver.find_element_by_id("android:id/big_text").get_attribute('text') #android:id/text2 to android:id/text
        OTP = re.findall(r'\d+', e1)
        print(OTP)
        NewOTP = max(OTP, key=len)
        print(NewOTP)
        driver.find_element_by_id("com.android.systemui:id/dismiss_view").click()
        print('OTP value:' + NewOTP)
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/pin_view")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/pin_view").send_keys(NewOTP)
        XLUtilities.writeData(path, "Info", 3, 7, NewOTP)
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/secondary_action_btn")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/secondary_action_btn").click()
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile").click()
except:
    pass


ele_Actual_username = "sg.gov.mnd.OneService:id/user_profile_userid"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_Actual_username)):
        Actual_username = driver.find_element_by_id(ele_Actual_username).get_attribute('text')
        print("Actual username: " + Actual_username)
        print("Expected username: " + Username)
        if Actual_username == Username:
            XLUtilities.writeData(path, "Info", 3, 4, "PASS")
            XLUtilities.writeData(path, "Info", 3, 2, Actual_username)
        else:
            XLUtilities.writeData(path, "Info", 3, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 3, 2, Actual_username)
except:
    pass

ele_Actual_MobileNo = "sg.gov.mnd.OneService:id/user_profile_mobileno"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_Actual_MobileNo)):
        Actual_MobileNo = driver.find_element_by_id(ele_Actual_MobileNo).get_attribute('text')
        print("Actual mobile number: " + Actual_MobileNo)
        print("Expected mobile number: " + str(mobileNum))
        if Actual_MobileNo == mobileNum:
            XLUtilities.writeData(path, "Info", 4, 4, "PASS")
            XLUtilities.writeData(path, "Info", 4, 2, Actual_MobileNo)
        else:
            XLUtilities.writeData(path, "Info", 4, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 4, 2, Actual_MobileNo)
except:
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B5"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot attached...")

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout").click()
except:
    pass

XLUtilities.close(path)
driver.quit()


