#!/usr/bin/env python
# -*- coding: utf-8 -*-
from appium import webdriver

def setUp():
    desired_caps = {
        'platformName': 'Android',
        #'appPackage': 'sg.gov.mnd.OneService',
        'deviceName': 'cfb59612',
        #'deviceName': '8LAE8LVCIBFAMNNF', #Redmi note 2
        #'deviceName': '3434593957553098',#Samsung S9
        #'deviceName': '10.100.3.62:5555',#Redmi 7Wifi
        #'deviceName': 'c2617e9c', #POCO
        #'udid': '3434593957553098',
        #'deviceId': '3434593957553098',
        #'automationName': 'UiAutomator1',
        'automationName': 'UiAutomator2',
        'noReset': 'true',
        'app': 'C:\\Users\\PCCW Solutions\\Documents\\ApkProjects\\Jenkins\\Appdome_oneservice-app-STAGING-release-v4.3.5-PDF.apk',
        'appPackage': 'sg.gov.mnd.OneService',
        'platformVersion': '9',
        'appWaitForLaunch': 'true',
        'skipServerInstallation': 'true'
        #'-–session-override': 'true'
        #'platformVersion': '5'
        #'platformVersion': '10',
        #'unicodeKeyboard': 'true',
        #'ignoreUnimportantViews': 'true',
        #disableAndroidWatchers': 'true'
        #'resetKeyboard': 'true'
    }
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)
    driver.implicitly_wait(3)
    return driver

if __name__ == '__main__':
    setUp()
