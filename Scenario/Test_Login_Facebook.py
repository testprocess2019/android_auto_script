import Test_capability
import time
import XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\User_Login.xlsx"
path2 = "C:\\logs\\Artefacts.png"

ele_log_in_fb_btn = "sg.gov.mnd.OneService:id/login_tab_fb_btn"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id(ele_log_in_fb_btn)):
        driver.find_element_by_id(ele_log_in_fb_btn).click()
except:
    pass

time.sleep(10)
ele_bottom_nav_profile = "sg.gov.mnd.OneService:id/bottom_navigation_profile"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_bottom_nav_profile)):
        driver.find_element_by_id(ele_bottom_nav_profile).click()
except:
    pass

Username = XLUtilities.readData(path, "Info", 10, 3)
ele_Username = "sg.gov.mnd.OneService:id/user_profile_userid"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_Username)):
        actual_username = driver.find_element_by_id(ele_Username).get_attribute("text")
        print("Actual username: " + actual_username)
        print("Expected username: " + str(Username))
        if actual_username == Username:
            XLUtilities.writeData(path, "Info", 10, 4, "PASS")
            XLUtilities.writeData(path, "Info", 10, 2, actual_username)
        else:
            XLUtilities.writeData(path, "Info", 10, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 10, 2, actual_username)
except:
    pass

mobileNum = XLUtilities.readData(path, "Info", 11, 3)
ele_mobileNum = "sg.gov.mnd.OneService:id/user_profile_mobileno"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_mobileNum)):
        actual_mobileNo = driver.find_element_by_id(ele_mobileNum).get_attribute("text")
        print("Actual username: " + actual_mobileNo)
        print("Expected username: " + str(mobileNum))
        if actual_mobileNo == str(mobileNum):
            XLUtilities.writeData(path, "Info", 11, 4, "PASS")
            XLUtilities.writeData(path, "Info", 11, 2, actual_mobileNo)
        else:
            XLUtilities.writeData(path, "Info", 11, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 11, 2, actual_mobileNo)
except:
    pass

driver.get_screenshot_as_file(path2)
loc1 = "B13"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot attached...")

ele_user_prof_log_out = "sg.gov.mnd.OneService:id/user_profile_item_logout"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_user_prof_log_out)):
        driver.find_element_by_id(ele_user_prof_log_out).click()
except:
    pass

XLUtilities.close(path)
driver.quit()