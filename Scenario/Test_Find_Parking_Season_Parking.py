import Test_capability
import time
#from appium import webdriver
import XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Find_Park_Info.xlsx"
path2 = "C:\\logs\\Artefacts.png"

time.sleep(30)

ele_Find_parking_Btn = "sg.gov.mnd.OneService:id/find_parking_module_icon"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_Find_parking_Btn)):
        driver.find_element_by_id(ele_Find_parking_Btn).click()
except:
    pass

time.sleep(10)
A = XLUtilities.readData(path, "Info", 7, 4)
ele_enter_address = "sg.gov.mnd.OneService:id/address_search_edit_text"
ele_address_value = "mnd"
# ele_address_value = "5 MAXWELL ROAD MINISTRY OF NATIONAL DEVELOPMENT (MND) SINGAPORE 069110"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_enter_address)):
        driver.find_element_by_id(ele_enter_address).click()
        driver.find_element_by_id(ele_enter_address).send_keys(ele_address_value)
        driver.find_element_by_id(ele_enter_address).click()
except:
    pass

ele_select_address_Btn = "sg.gov.mnd.OneService:id/list_content"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_select_address_Btn)):
        driver.find_element_by_id(ele_select_address_Btn).click()
except:
    pass

# Click filter button
ele_filter_Btn = "sg.gov.mnd.OneService:id/filter_btn"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_filter_Btn)):
        driver.find_element_by_id(ele_filter_Btn).click()
except:
    pass

# Uncheck the exclude season parking checkbox
ele_checkbox = "sg.gov.mnd.OneService:id/chkbox"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_checkbox)):
        driver.find_element_by_id(ele_checkbox).click()
except:
    pass

# Click apply button
ele_apply_btn = "sg.gov.mnd.OneService:id/filter_apply_btn"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_apply_btn)):
        driver.find_element_by_id(ele_apply_btn).click()
except:
    pass

time.sleep(10)

Exp_season_parking_list_info = XLUtilities.readData(path, "Info", 7, 4)
ele_select_cheapest_season_parking = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[1]"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_xpath(ele_select_cheapest_season_parking)):
        Act_season_parking_list_info = driver.find_element_by_xpath(ele_select_cheapest_season_parking).get_attribute('text')
        print("Actual season parking list info: " + Act_season_parking_list_info)
        print("Expected season parking list info: " + Exp_season_parking_list_info)
        if Act_season_parking_list_info == Exp_season_parking_list_info:
            XLUtilities.writeData(path, "Info", 7, 6, "PASS")
            XLUtilities.writeData(path, "Info", 7, 2, Act_season_parking_list_info)
        else:
            XLUtilities.writeData(path, "Info", 7, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 7, 2, Act_season_parking_list_info)
        driver.find_element_by_xpath(ele_select_cheapest_season_parking).click()
except:
    pass

time.sleep(20)
driver.get_screenshot_as_file(path2)
# 8 Verify season parking info(title) :
Exp_season_parking_title = XLUtilities.readData(path, "Info", 8, 4)
ele_season_parking_title = "sg.gov.mnd.OneService:id/details_address_holder"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_season_parking_title)):
        Act_season_parking_title = driver.find_element_by_id(ele_season_parking_title).get_attribute('text')
        print("Actual season parking title: " + Act_season_parking_title)
        print("Expected season parking title: " + Exp_season_parking_title)
        if Act_season_parking_title == Exp_season_parking_title:
            XLUtilities.writeData(path, "Info", 8, 6, "PASS")
            XLUtilities.writeData(path, "Info", 8, 2, Act_season_parking_title)
            loc1 = "H7"
            XLUtilities.InsertImg(path, loc1, path2)
        else:
            XLUtilities.writeData(path, "Info", 8, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 8, 2, Act_season_parking_title)
            loc1 = "H7"
            XLUtilities.InsertImg(path, loc1, path2)
except:
    pass

# 9 Verify season parking info(cost title) :
Exp_season_parking_cost_title = XLUtilities.readData(path, "Info", 9, 4)
ele_season_parking_cost_title = "sg.gov.mnd.OneService:id/tvTotalCost"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_season_parking_cost_title)):
        Act_season_parking_cost_title = driver.find_element_by_id(ele_season_parking_cost_title).get_attribute('text')
        print("Actual season parking cost title: " + Act_season_parking_cost_title)
        print("Expected season parking cost title: " + Exp_season_parking_cost_title)
        if Act_season_parking_cost_title == Exp_season_parking_cost_title:
            XLUtilities.writeData(path, "Info", 9, 6, "PASS")
            XLUtilities.writeData(path, "Info", 9, 2, Act_season_parking_cost_title)
        else:
            XLUtilities.writeData(path, "Info", 9, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 9, 2, Act_season_parking_cost_title)
except:
    pass

driver.swipe(301, 1280, 326, 342, 400)
time.sleep(5)
driver.get_screenshot_as_file(path2)
# 10 Verify commercial parking info:
Exp_Commercial_parking_info = XLUtilities.readData(path, "Info", 10, 4)
ele_Commercial_parking_info = "sg.gov.mnd.OneService:id/details_code_holder"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_Commercial_parking_info)):
        Act_Commercial_parking_info = driver.find_element_by_id(ele_Commercial_parking_info).get_attribute('text')
        print("Actual commercial parking title: " + Act_Commercial_parking_info)
        print("Expected commercial parking title: " + Exp_Commercial_parking_info)
        if Act_Commercial_parking_info == Exp_Commercial_parking_info:
            XLUtilities.writeData(path, "Info", 10, 6, "PASS")
            XLUtilities.writeData(path, "Info", 10, 2, Act_Commercial_parking_info)
            loc1 = "H10"
            XLUtilities.InsertImg(path, loc1, path2)
        else:
            XLUtilities.writeData(path, "Info", 10, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 10, 2, Act_Commercial_parking_info)
            loc1 = "H10"
            XLUtilities.InsertImg(path, loc1, path2)
except:
    pass


# 11 Verify vehicle pricing detail:
Exp_Vehicle_pricing_detail = XLUtilities.readData(path, "Info", 11, 4)
ele_Vehicle_pricing_detail = "sg.gov.mnd.OneService:id/vehicle_pricing_details"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_Vehicle_pricing_detail)):
        Act_Vehicle_pricing_detail = driver.find_element_by_id(ele_Vehicle_pricing_detail).get_attribute('text')
        print("Actual season parking pricing detail: " + Act_Vehicle_pricing_detail)
        print("Expected season parking pricing detail: " + Exp_Vehicle_pricing_detail)
        if Act_Vehicle_pricing_detail == Exp_Vehicle_pricing_detail:
            XLUtilities.writeData(path, "Info", 11, 6, "PASS")
            XLUtilities.writeData(path, "Info", 11, 2, Act_Vehicle_pricing_detail)
        else:
            XLUtilities.writeData(path, "Info", 11, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 11, 2, Act_Vehicle_pricing_detail)
except:
    pass


# 12 Verify opening hours:
Exp_open_hours = XLUtilities.readData(path, "Info", 12, 4)
ele_open_hours = "sg.gov.mnd.OneService:id/opening_hours_info"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_open_hours)):
        Act_open_hours = driver.find_element_by_id(ele_open_hours).get_attribute('text')
        print("Actual season parking open hours: " + Act_open_hours)
        print("Expected season parking open hours: " + Exp_open_hours)
        if Act_open_hours == Exp_open_hours:
            XLUtilities.writeData(path, "Info", 12, 6, "PASS")
            XLUtilities.writeData(path, "Info", 12, 2, Act_open_hours)
        else:
            XLUtilities.writeData(path, "Info", 12, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 12, 2, Act_open_hours)
except:
    pass


# 13 Verify height limit:
Exp_height_limit = XLUtilities.readData(path, "Info", 13, 4)
ele_height_limit = "sg.gov.mnd.OneService:id/height_value"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_height_limit)):
        Act_height_limit = driver.find_element_by_id(ele_height_limit).get_attribute('text')
        print("Actual season parking height limit: " + Act_height_limit)
        print("Expected season parking height limit: " + Exp_height_limit)
        if Act_height_limit == Exp_height_limit:
            XLUtilities.writeData(path, "Info", 13, 6, "PASS")
            XLUtilities.writeData(path, "Info", 13, 2, Act_height_limit)
        else:
            XLUtilities.writeData(path, "Info", 13, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 13, 2, Act_height_limit)
except:
    pass


# 14 Verify the height limit other info :
Exp_height_limit_other_info = XLUtilities.readData(path, "Info", 14, 4)
ele_height_limit_other_info = "sg.gov.mnd.OneService:id/vehicle_height_other_info"
try:
    if WebDriverWait(driver, 30).until(lambda x: x.find_element_by_id(ele_height_limit_other_info)):
        Act_height_limit_other_info = driver.find_element_by_id(ele_height_limit_other_info).get_attribute('text')
        print("Actual season parking other info: " + Act_height_limit_other_info)
        print("Expected season parking other info: " + Exp_height_limit_other_info)
        if Act_height_limit_other_info == Exp_height_limit_other_info:
            XLUtilities.writeData(path, "Info", 14, 6, "PASS")
            XLUtilities.writeData(path, "Info", 14, 2, Act_height_limit_other_info)
            loc1 = "H10"
            XLUtilities.InsertImg(path, loc1, path2)
        else:
            XLUtilities.writeData(path, "Info", 14, 6, "FAILED")
            XLUtilities.writeData(path, "Info", 14, 2, Act_height_limit_other_info)
            loc1 = "H10"
            XLUtilities.InsertImg(path, loc1, path2)
except:
    pass

time.sleep(5)
XLUtilities.close(path)
driver.quit()