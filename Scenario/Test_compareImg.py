from PIL import Image
from PIL import ImageChops
#from PIL import ImageGrab
#import Test_capability
from selenium.webdriver.support.wait import WebDriverWait

#driver = Test_capability.setUp()

def Test_CompareImg(driver):
    path = "C:\\Users\\PCCW Solutions\Documents\\Test Image\\MSOLogo.png"
    path2 = "C:\\Users\\PCCW Solutions\Documents\\Test Image\\ActMSOLogo1.png"
    path3 = "C:\\Users\\PCCW Solutions\Documents\\Test Image\\HomeIcon.png"
    path4 = "C:\\Users\\PCCW Solutions\Documents\\Test Image\\ActHomeIcon1.png"

    try:
        if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/mso_logo")):
            ele_logo = driver.find_element_by_id("sg.gov.mnd.OneService:id/mso_logo")
    except:
        pass

    try:
        if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_xpath(
                "//android.widget.FrameLayout[@content-desc='HOME']/android.widget.ImageView")):
            ele_Home = driver.find_element_by_xpath(
                "//android.widget.FrameLayout[@content-desc='HOME']/android.widget.ImageView")
    except:
        pass

    driver.get_screenshot_as_file(path2)
    driver.get_screenshot_as_file(path4)

    Logo_location = ele_logo.location
    Logo_size = ele_logo.size
    Logo_box = (Logo_location["x"], Logo_location["y"], Logo_location["x"] + Logo_size["width"], \
                Logo_location["y"] + Logo_size["height"])

    Home_location = ele_Home.location
    Home_size = ele_Home.size
    Home_box = (Home_location["x"], Home_location["y"], Home_location["x"] + Home_size["width"], \
                Home_location["y"] + Home_size["height"])

    Logo_image = Image.open(path2)
    new_Logo_Image = Logo_image.crop(Logo_box)
    new_Logo_Image.save(path2)

    Home_image = Image.open(path4)
    new_Home_Image = Home_image.crop(Logo_box)
    new_Home_Image.save(path4)

    img1_Logo = Image.open(path)
    # new_img1 = img1.resize((135, 135))
    img2_Logo = Image.open(path2)
    # new_img2 = img2.resize((135, 135))

    img1_Home = Image.open(path3)
    img2_Home = Image.open(path4)

    diff_Logo = ImageChops.difference(img1_Logo, img2_Logo)
    diff_Home = ImageChops.difference(img1_Home, img2_Home)

    if diff_Logo.getbbox():
        Logo_Test_Result = "Logo images are different"
        print(Logo_Test_Result)
    else:
        Logo_Test_Result = "Logo images are same"
        print(Logo_Test_Result)

    if diff_Home.getbbox():
        Home_Test_Result = "Home images are different"
        print(Home_Test_Result)
    else:
        Home_Test_Result = "Home images are same"
        print(Home_Test_Result)

    return Logo_Test_Result, Home_Test_Result
'''
if __name__ == '__main__':
    Test_CompareImg(driver)
'''
