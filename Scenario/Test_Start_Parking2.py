import Test_capability
import time
from selenium.webdriver.support.ui import WebDriverWait
import XLUtilities

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Start_Park_Info.xlsx"
path2 = "C:\\logs\\Artefacts.png"

time.sleep(30)

#Launch start parking page:
ele_start_parking_page = "sg.gov.mnd.OneService:id/eparking_module_icon"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_start_parking_page)):
        driver.find_element_by_id(ele_start_parking_page).click()
except:
    pass
#Allow access this device location
ele_device_location = "com.android.packageinstaller:id/permission_allow_button"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_device_location)):
        driver.find_element_by_id(ele_device_location).click()
except:
    pass

#Allow access this device location
ele_device_Access_location = "com.android.packageinstaller:id/permission_allow_button"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_device_Access_location)):
        driver.find_element_by_id(ele_device_Access_location).click()
except:
    pass

#click locate button:
ele_locator = "sg.gov.mnd.OneService:id/locate_btn"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_locator)):
        driver.find_element_by_id(ele_locator).click()
except:
    pass

#Start parking page:

#1. Parking duration:
expected_parking_duration = XLUtilities.readData(path, "Info", 3, 3)
ele_parking_duration = "sg.gov.mnd.OneService:id/parking_duration_mins"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_parking_duration)):
        Actual_Parking_duration = driver.find_element_by_id(ele_parking_duration).get_attribute('text')
        print("Actual parking duration: " + Actual_Parking_duration)
        print("Expected packing duration: " + str(expected_parking_duration))
        if Actual_Parking_duration == expected_parking_duration:
            XLUtilities.writeData(path, "Info", 3, 4, "PASS")
            XLUtilities.writeData(path, "Info", 3, 2, Actual_Parking_duration)
        else:
            XLUtilities.writeData(path, "Info", 3, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 3, 2, Actual_Parking_duration)
except:
    pass


#Select car park
ele_select_parking_car_park = "sg.gov.mnd.OneService:id/parking_carpark"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_select_parking_car_park)):
        driver.find_element_by_id(ele_select_parking_car_park).click()
except:
    pass

#wait for all nearest car park info displayed
time.sleep(10)

# Select nearest car park
ele_select_nearest_car_park = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_select_nearest_car_park)):
        driver.find_element_by_xpath(ele_select_nearest_car_park).click()
except:
    pass

#Select car
ele_select_car = "sg.gov.mnd.OneService:id/parking_vehicle_plate"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_select_car)):
        driver.find_element_by_id(ele_select_car).click()
except:
    pass

#Select car with particular vehicle plate
ele_car_plate_number = "//android.widget.TextView[1][@text='TEST']"
#ele_car_plate_number = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup[3]/android.widget.TextView[1][@text='TEST']"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_car_plate_number)):
        driver.find_element_by_xpath(ele_car_plate_number).click()
except:
    pass

#3. Verifiy start parking page displayed:
expected_parking_header = XLUtilities.readData(path, "Info", 5, 3)
ele_parking_header = "sg.gov.mnd.OneService:id/tvHeader"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_parking_header)):
        Actual_parking_header = driver.find_element_by_id(ele_parking_header).get_attribute('text')
        print("Actual start parking page title: " + Actual_parking_header)
        print("Expected start parking page title: " + expected_parking_header)
        if Actual_parking_header == expected_parking_header:
            XLUtilities.writeData(path, "Info", 5, 4, "PASS")
            XLUtilities.writeData(path, "Info", 5, 2, Actual_parking_header)
        else:
            XLUtilities.writeData(path, "Info", 5, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 5, 2, Actual_parking_header)
except:
    pass

#2. Total cost:
expected_total_cost = XLUtilities.readData(path, "Info", 4, 3)
ele_total_cost = "sg.gov.mnd.OneService:id/parking_cost"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_total_cost)):
        Actual_total_cost = driver.find_element_by_id(ele_total_cost).get_attribute('text')
        print("Actual total cost: " + Actual_total_cost)
        print("Expected total cost: " + str(expected_total_cost))
        if Actual_total_cost == expected_total_cost:
            XLUtilities.writeData(path, "Info", 4, 4, "PASS")
            XLUtilities.writeData(path, "Info", 4, 2, Actual_total_cost)
        else:
            XLUtilities.writeData(path, "Info", 4, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 4, 2, Actual_total_cost)
except:
    pass


#4. Verifiy select nearest car park:
expected_nearest_parking = XLUtilities.readData(path, "Info", 6, 3)
ele_nearest_parking = "sg.gov.mnd.OneService:id/parking_carpark"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_nearest_parking)):
        Actual_nearest_parking = driver.find_element_by_id(ele_nearest_parking).get_attribute('text')
        print("Actual nearest car park: " + Actual_nearest_parking)
        print("Expected nearest car park: " + expected_nearest_parking)
        if Actual_nearest_parking == expected_nearest_parking:
            XLUtilities.writeData(path, "Info", 6, 4, "PASS")
            XLUtilities.writeData(path, "Info", 6, 2, Actual_nearest_parking)
        else:
            XLUtilities.writeData(path, "Info", 6, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 6, 2, Actual_nearest_parking)
except:
    pass


#5. Verify vehicle number:
expected_vehicle_number = XLUtilities.readData(path, "Info", 7, 3)
ele_vehicle_number = "sg.gov.mnd.OneService:id/parking_vehicle_plate"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_vehicle_number)):
        Actual_vehicle_number = driver.find_element_by_id(ele_vehicle_number).get_attribute('text')
        print("Actual vehicle number: " + Actual_vehicle_number)
        print("Expected vehicle number: " + expected_vehicle_number)
        if Actual_vehicle_number == expected_vehicle_number:
             XLUtilities.writeData(path, "Info", 7, 4, "PASS")
             XLUtilities.writeData(path, "Info", 7, 2, Actual_vehicle_number)
        else:
            XLUtilities.writeData(path, "Info", 7, 4, "FAILED")
            XLUtilities.writeData(path, "Info", 7, 2, Actual_vehicle_number)
except:
    pass
time.sleep(10)
driver.get_screenshot_as_file(path2)
loc1 = "B8"
XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot attached...")

XLUtilities.close(path)
driver.quit()

