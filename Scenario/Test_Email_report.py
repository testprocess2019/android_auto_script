import smtplib
from email.message import EmailMessage
import time
from datetime import datetime
dateTimeObj = datetime.now()
SenderEmail = 'Testprocess2019@gmail.com'
EmailPwd = 'P@$$word123'
ReceiverEmail = 'Cheng-Wee.Woon@PCCW.com'
contacts = ['Cheng-Wee.Woon@PCCW.com', 'domainpasar@gmail.com']
#ted_kwek@mnd.gov.sg
timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S)")
print('Current Timestamp : ', timestampStr)
subj = ("Automated Test Result-OSAPP(Android) " + timestampStr)

'''
smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
smtpserver.ehlo()
smtpserver.starttls()
smtpserver.ehlo()
smtpserver.login(SenderEmail, EmailPwd)
subject = 'Appium Demo Test Result'
body = 'Testing was completed'
msg = f'Subject: {subject}\n\n{body}'
smtpserver.sendmail(SenderEmail, ReceiverEmail, msg)
'''

msg = EmailMessage()
#msg['Subject'] = 'Automated Test Result'
msg['Subject'] = subj
msg['From'] = SenderEmail
#msg['To'] = ReceiverEmail # Single receiver
msg['To'] = ', '.join(contacts) # multiple receiver
msg.set_content('Hi All,\nAutomated test for OSAPP-Android was completed, kindly find the report from attached.\n\nThanks n regards,\nPCCW Support Team')

report1 = 'C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Start_Park_Info.xlsx'
report2 = 'C:\\Users\\PCCW Solutions\\Documents\\Test Script\\User_Login.xlsx'
report3 = 'C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Submit_Case.xlsx'
report4 = 'C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Find_Park_Info.xlsx'
report5 = 'C:\\Users\\PCCW Solutions\\Documents\\Test Script\\History_Info.xlsx'
files = [report1, report2, report3, report4, report5]

for file in files:
    with open(file, 'rb') as f:
        file_data = f.read()
        file_name = f.name
    msg.add_attachment(file_data, maintype='application', subtype='octet-stream', filename=file_name)

smtpserver = smtplib.SMTP_SSL("smtp.gmail.com", 465)
smtpserver.login(SenderEmail, EmailPwd)
smtpserver.send_message(msg)

print("Email sent succcessfully")
