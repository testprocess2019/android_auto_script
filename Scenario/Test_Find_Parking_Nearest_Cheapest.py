import Test_capability
import time
#from appium import webdriver
import XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_capability.setUp()
path = "C:\\Users\\PCCW Solutions\\Documents\\Test Script\\Find_Park_Info.xlsx"
path2 = "C:\\logs\\Artefacts.png"
rows = XLUtilities.getRowCount(path, "Info")

time.sleep(30)

# Launch find parking page:

#Click find parking button
ele_Find_parking_Btn = "sg.gov.mnd.OneService:id/find_parking_module_icon"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_Find_parking_Btn)):
        driver.find_element_by_id(ele_Find_parking_Btn).click()
except:
    pass

#Click location icon
ele_locate_icon = "sg.gov.mnd.OneService:id/locate_btn"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_locate_icon)):
        driver.find_element_by_id(ele_locate_icon).click()
except:
    pass

#Click find parking icon
ele_Find_parking_icon = "sg.gov.mnd.OneService:id/find_parking_button"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_Find_parking_icon)):
        driver.find_element_by_id(ele_Find_parking_icon).click()
except:
    pass

time.sleep(5)
#1.1 click sort by cheapest:
ele_cheapest_Btn = "sg.gov.mnd.OneService:id/sort_by_cheapest"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_cheapest_Btn)):
        driver.find_element_by_id(ele_cheapest_Btn).click()
except:
    pass

time.sleep(5)
for r in range(18, rows + 1):
    Expected_Address_C = XLUtilities.readData(path, "Info", r, 4)
    Expected_Park_fee_C = XLUtilities.readData(path, "Info", r, 5)
    Postal_code = XLUtilities.readData(path, "Info", r, 3)
    print(Expected_Address_C)
    if Expected_Address_C == None:
        print("file finished")
        break

    ele_el10 = "sg.gov.mnd.OneService:id/address_search_edit_text"
    try:
        if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(ele_el10)):
            test_1 = driver.find_element_by_id(ele_el10).click()
            test_1.click()
    except:
        pass

    el10_0 = "sg.gov.mnd.OneService:id/clear_text"
    try:
        if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(el10_0)):
            driver.find_element_by_id(el10_0).click()
    except:
        pass

    ele_el10_1 = "sg.gov.mnd.OneService:id/address_search_edit_text"
    try:
        if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(ele_el10_1)):
            test_2 = driver.find_element_by_id(ele_el10_1)
            test_2.send_keys(Postal_code)
            test_2.click()
    except:
        pass

    time.sleep(5)
    ele_cheapest_car_park_item = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView"
    try:
        if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_xpath(ele_cheapest_car_park_item)):
            driver.find_element_by_xpath(ele_cheapest_car_park_item).click()
    except:
        pass
    time.sleep(10)
    driver.get_screenshot_as_file(path2)
    ele_el4 = "sg.gov.mnd.OneService:id/address_holder"
    try:
        if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(ele_el4)):
            el4 = driver.find_element_by_id(ele_el4).get_attribute('text')
            print("Actual cheapest packing: " + el4)
            print("Expected cheapest packing: " + Expected_Address_C + ",Postal code: " + str(Postal_code))
            if el4 == Expected_Address_C or el4 is not None:
                XLUtilities.writeData(path, "Info", r, 6, "PASS")
                XLUtilities.writeData(path, "Info", r, 1, el4)
                loc1 = ("H" + str(r))
                XLUtilities.InsertImg(path, loc1, path2)
            else:
                XLUtilities.writeData(path, "Info", r, 6, "FAILED")
                XLUtilities.writeData(path, "Info", r, 1, el4)
                loc1 = ("H" + str(r))
                XLUtilities.InsertImg(path, loc1, path2)
    except:
        loc1 = ("H" + str(r))
        XLUtilities.InsertImg(path, loc1, path2)
        pass

    ele_el5 = "sg.gov.mnd.OneService:id/price_holder"
    try:
        if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(ele_el5)):
            el5 = driver.find_element_by_id(ele_el5).get_attribute('text')
            print("Actual cheapest packing fee: " + el5)
            print("Expected cheapest packing fee " + str(Expected_Park_fee_C))
            if el5 == Expected_Park_fee_C or el5 is not None:
                XLUtilities.writeData(path, "Info", r, 6, "PASS")
                XLUtilities.writeData(path, "Info", r, 2, el5)
            else:
                XLUtilities.writeData(path, "Info", r, 6, "FAILED")
                XLUtilities.writeData(path, "Info", r, 2, el5)
    except:
        pass

    ele_el10_2 = "sg.gov.mnd.OneService:id/address_search_edit_text"
    try:
        if WebDriverWait(driver, 5).until(lambda x: x.find_element_by_id(ele_el10_2)):
            driver.find_element_by_id(ele_el10_2).clear()
    except:
        pass

time.sleep(10)
#1.2 click sort by Nearest:
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/sort_by_nearest")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/sort_by_nearest").click()
except:
    pass

time.sleep(5)
for r in range(22, rows + 1):
    Expected_Address_N = XLUtilities.readData(path, "Info", r, 4)
    Expected_Park_fee_N = XLUtilities.readData(path, "Info", r, 5)
    Postal_code_N = XLUtilities.readData(path, "Info", r, 3)
    print(Expected_Address_N)
    if Expected_Address_N == None:
        print("file finished")
        break
    ele_el7 = "sg.gov.mnd.OneService:id/address_search_edit_text"
    try:
        if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_el7)):
            Test_3 = driver.find_element_by_id(ele_el7).click()
            Test_3.click()
    except:
        pass

    ele_el7_0 = "sg.gov.mnd.OneService:id/clear_text"
    try:
        if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_el7_0)):
            driver.find_element_by_id(ele_el7_0).click()
    except:
        pass

    ele_el7_1 = "sg.gov.mnd.OneService:id/address_search_edit_text"
    try:
        if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_el7_1)):
            Test_4 = driver.find_element_by_id(ele_el7_1).send_keys(Postal_code_N)
            Test_4.click()

    except:
        pass
    time.sleep(5)
    ele_nearest_car_park_item = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView"
    try:
        if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_nearest_car_park_item)):
            driver.find_element_by_xpath(ele_nearest_car_park_item).click()
    except:
        pass

    time.sleep(10)
    driver.get_screenshot_as_file(path2)
    ele_el8 = "sg.gov.mnd.OneService:id/address_holder"
    try:
        if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_el8)):
            el8 = driver.find_element_by_id(ele_el8).get_attribute('text')
            print("Actual nearest packing: " + el8)
            print("Expected nearest packing: " + Expected_Address_N + ",Postal code: " + str(Postal_code_N))
            if el8 == Expected_Address_N or el8 is not None:
                XLUtilities.writeData(path, "Info", r, 6, "PASS")
                XLUtilities.writeData(path, "Info", r, 1, el8)
                loc1 = ("H" + str(r))
                XLUtilities.InsertImg(path, loc1, path2)
            else:
                XLUtilities.writeData(path, "Info", r, 6, "FAILED")
                XLUtilities.writeData(path, "Info", r, 1, el8)
                loc1 = ("H" + str(r))
                XLUtilities.InsertImg(path, loc1, path2)
    except:
        loc1 = ("H" + str(r))
        XLUtilities.InsertImg(path, loc1, path2)
        pass

    ele_el9 = "sg.gov.mnd.OneService:id/price_holder"
    try:
        if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_el9)):
            el9 = driver.find_element_by_id(ele_el9).get_attribute('text')
            print("Actual nearest packing fee: " + el9)
            print("Expected nearest packing fee: " + Expected_Park_fee_N)
            if el9 == Expected_Park_fee_N or el9 is not None:
                XLUtilities.writeData(path, "Info", r, 6, "PASS")
                XLUtilities.writeData(path, "Info", r, 2, el9)
            else:
                XLUtilities.writeData(path, "Info", r, 6, "FAILED")
                XLUtilities.writeData(path, "Info", r, 2, el9)
    except:
        pass

    ele_el7_2 = "sg.gov.mnd.OneService:id/address_search_edit_text"
    try:
        if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id(ele_el7_2)):
            driver.find_element_by_id(ele_el7_2).clear()
    except:
        pass

XLUtilities.close(path)
driver.quit()
