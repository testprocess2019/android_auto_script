# coding:utf-8
import base64
import time
import appium
from appium import webdriver

PATH = lambda p: os.path.abspath(os.path.dirname(__file__), p)


class ContactAndroidTests(unittest.TestCase):
    def setUp(self):
        def setUp(self):
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['deviceName'] = 'device'
            desired_caps['noReset'] = 'true'
            desired_caps['app'] = PATH(
                'C:\\Users\\PCCW Solutions\\AppData\\Local\\Android\\Sdk\\platform-tools\\sg.gov.mnd.OneService_411_apkplz.net.apk')
            desired_caps['appPackage'] = 'sg.gov.mnd.OneService'
            desired_caps['getCurrentActivity'] = '.PrincipalActivity.PrincipalActivity'

            self.driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)

    def tearDown(self):
        self.driver.quit()

    def test_button(self):
        el1 = self.driver.find_element_by_id("sg.gov.mnd.OneService:id/submit_case_module_icon")
        el1.click()
        el2 = self.driver.find_element_by_id("sg.gov.mnd.OneService:id/eparking_module_icon")
        el2.click()
        el3 = self.driver.find_element_by_id("sg.gov.mnd.OneService:id/find_parking_module_icon")
        el3.click()