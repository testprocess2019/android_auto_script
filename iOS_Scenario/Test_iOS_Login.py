# coding:utf-8
import time
import Test_iOS_capability
import iOS_XLUtilities
from selenium.webdriver.support.ui import WebDriverWait
import re


driver = Test_iOS_capability.setUp()
path = "/Users/kuanwah/Documents/Test Script/User_Info.xlsx"
rows = iOS_XLUtilities.getRowCount(path, "Info")
Username = iOS_XLUtilities.readData(path, "Info", 2, 1)
mobileNum = iOS_XLUtilities.readData(path, "Info", 2, 2)

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id('Enter User ID')):
        el1 = driver.find_element_by_accessibility_id('Enter User ID')
        el1.send_keys(Username)
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id('Enter Mobile Number')):
        el2 = driver.find_element_by_accessibility_id('Enter Mobile Number')
        el2.send_keys(mobileNum)
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id('btn login enabled')):
        el3 = driver.find_element_by_accessibility_id('btn login enabled')
        if el3.is_enabled():
            el3.click()
        else:
            pass
except:
    pass

time.sleep(10)
#driver.open_notifications()

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id('Typing Predictions')):
        driver.find_element_by_find_element_by_accessibility_id('Typing Predictions').click()
except:
    pass
'''
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id('Typing Predictions')):
        e1 = driver.find_element_by_id("android:id/text2").get_attribute('text')
        OTP = re.findall(r'\d+', e1)
        print(OTP)
        NewOTP = max(OTP, key=len)
        print(NewOTP)
        driver.find_element_by_id("com.android.systemui:id/clear_all_button").click()
        print(NewOTP)
except:
    pass

try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/pin_view")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/pin_view").send_keys(NewOTP)
except:
    pass
'''
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/bottom_navigation_profile").click()
except:
    pass

try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout")):
        driver.find_element_by_id("sg.gov.mnd.OneService:id/user_profile_item_logout").click()
except:
    pass




