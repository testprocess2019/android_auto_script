import Test_iOS_capability
import time
import iOS_XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_iOS_capability.setUp()
path = '//Users//Admin/Documents//Test Script//User_Login.xlsx'
Image_path = '//Users//kuanwah//Documents//Test Image//Artefacts.png'

ele_log_in_fb_btn = "btn facebookLogin"
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_accessibility_id(ele_log_in_fb_btn)):
        driver.find_element_by_accessibility_id(ele_log_in_fb_btn).click()
except:
    pass


#Click "Continue" on the pop up message
ele_continue_btn = "Continue"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_continue_btn)):
        driver.find_element_by_accessibility_id(ele_continue_btn).click()
except:
    pass


#Click "Continue" on the facebook log in page
ele_continue_btn2 = "Continue"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_continue_btn2)):
        driver.find_element_by_accessibility_id(ele_continue_btn2).click()
except:
    pass

time.sleep(10)
#Click "Profile" icon
ele_profile_icon_btn = '//XCUIElementTypeButton[4]'
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_profile_icon_btn)):
        driver.find_element_by_xpath(ele_profile_icon_btn).click()
except:
    pass

time.sleep(5)
driver.get_screenshot_as_file(Image_path)
loc1 = 'B13'
iOS_XLUtilities.InsertImg(path, loc1, Image_path)
print('Screenshot for user profile was attached...')

Username = iOS_XLUtilities.readData(path, "Info", 11, 3)
ele_Username = "onepunch"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_Username)):
        actual_username = driver.find_element_by_accessibility_id(ele_Username).get_attribute("name")
        print("Actual username: " + actual_username)
        print("Expected username: " + str(Username))
        if actual_username == Username:
            iOS_XLUtilities.writeData(path, "Info", 11, 4, "PASS")
            iOS_XLUtilities.writeData(path, "Info", 11, 2, actual_username)
        else:
            iOS_XLUtilities.writeData(path, "Info", 11, 4, "FAILED")
            iOS_XLUtilities.writeData(path, "Info", 11, 2, actual_username)
except:
    pass

mobileNum = iOS_XLUtilities.readData(path, "Info", 12, 3)
ele_mobileNum = "86771397"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_mobileNum)):
        actual_mobileNo = driver.find_element_by_accessibility_id(ele_mobileNum).get_attribute("name")
        print("Actual username: " + actual_mobileNo)
        print("Expected username: " + str(mobileNum))
        if actual_mobileNo == str(mobileNum):
            iOS_XLUtilities.writeData(path, "Info", 12, 4, "PASS")
            iOS_XLUtilities.writeData(path, "Info", 12, 2, actual_mobileNo)
        else:
            iOS_XLUtilities.writeData(path, "Info", 12, 4, "FAILED")
            iOS_XLUtilities.writeData(path, "Info", 12, 2, actual_mobileNo)
except:
    pass


ele_user_prof_log_out = "Log Out"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_user_prof_log_out)):
        driver.find_element_by_accessibility_id(ele_user_prof_log_out).click()
except:
    pass

iOS_XLUtilities.close(path)
driver.quit()