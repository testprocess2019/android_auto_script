import Test_iOS_capability
import time
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_iOS_capability.setUp()

ele_Btn_Find_parking = "//XCUIElementTypeOther[3]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_Btn_Find_parking)):
        driver.find_element_by_xpath(ele_Btn_Find_parking).click()
except:
    pass

time.sleep(10)

driver.quit()