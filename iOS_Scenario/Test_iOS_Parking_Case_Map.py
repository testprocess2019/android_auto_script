import Test_iOS_capability
import time
import iOS_XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_iOS_capability.setUp()
path = "//Users//Admin/Documents//Test Script//Find_Park_Info.xlsx"
path2 = "//Users//kuanwah//Documents//Test Image//Artefacts.png"


# Launch find parking page:
# Click find parking button
# ele_Find_parking_Btn = "btn-find-parking"

time.sleep(10)
ele_Btn_Find_parking = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_Btn_Find_parking)):
        driver.find_element_by_xpath(ele_Btn_Find_parking).click()
except:
    pass

time.sleep(10)
#Close tooltip
ele_tips_holder = "Select time"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_tips_holder)):
        driver.find_element_by_accessibility_id(ele_tips_holder).click()
except:
    pass


time.sleep(10)
# Click location icon
ele_locate_icon = "btn location"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_locate_icon)):
        driver.find_element_by_accessibility_id(ele_locate_icon).click()
except:
    pass

time.sleep(10)
# Click case map icon
ele_case_map_icon = "//XCUIElementTypeButton[1]"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_case_map_icon)):
        driver.find_element_by_accessibility_id(ele_case_map_icon).click()
except:
    pass

time.sleep(10)
# Click on all catergory dropdown list
ele_all_cat_dropdown = "All Categories"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_all_cat_dropdown)):
        driver.find_element_by_accessibility_id(ele_all_cat_dropdown).click()
except:
    pass

time.sleep(10)
# select a category from dropdownlist
ele_test_3 = "//XCUIElementTypeStaticText[@name='Cleanliness']"
ele_cat_from_excel = iOS_XLUtilities.readData(path, "Info", 3, 4)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_test_3)):
        el_3 = driver.find_element_by_xpath(ele_test_3).get_attribute('name')
        driver.find_element_by_xpath(ele_test_3).click()
        print('Selected catergory: ' + el_3)
        if el_3 == ele_cat_from_excel:
            iOS_XLUtilities.writeData(path, "Info", 3, 6, "PASS")
            iOS_XLUtilities.writeData(path, "Info", 3, 2, el_3)
        else:
            iOS_XLUtilities.writeData(path, "Info", 3, 6, "FAILED")
            iOS_XLUtilities.writeData(path, "Info", 3, 2, el_3)
except:
    pass

time.sleep(10)
# Click on all status dropdown list
ele_all_status_dropdown = "All Statuses"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_all_status_dropdown)):
        driver.find_element_by_accessibility_id(ele_all_status_dropdown).click()
except:
    pass

time.sleep(10)
# select case status from dropdownlistMS
ele_test_4 = "//XCUIElementTypeStaticText[@name='Working On It']"
ele_status_from_excel = iOS_XLUtilities.readData(path, "Info", 4, 4)
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_test_4)):
        el_4 = driver.find_element_by_xpath(ele_test_4).get_attribute('text')
        driver.find_element_by_xpath(ele_test_4).click()
        print('Selected status: ' + el_4)
        if el_4 == ele_status_from_excel:
            iOS_XLUtilities.writeData(path, "Info", 4, 6, "PASS")
            iOS_XLUtilities.writeData(path, "Info", 4, 2, el_4)
        else:
            iOS_XLUtilities.writeData(path, "Info", 4, 6, "FAILED")
            iOS_XLUtilities.writeData(path, "Info", 4, 2, el_4)
except:
    pass
'''
# Click location icon
ele_locate_icon2 = "sg.gov.mnd.OneService:id/locate_btn"
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(ele_locate_icon2)):
        driver.find_element_by_accessibility_id(ele_locate_icon2).click()
except:
    pass
'''
driver.get_screenshot_as_file(path2)
loc1 = "H3"
iOS_XLUtilities.InsertImg(path, loc1, path2)
print("Screenshot attached...")

time.sleep(5)

iOS_XLUtilities.close(path)
driver.quit()
