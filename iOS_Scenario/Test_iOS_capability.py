#!/usr/bin/env python
# -*- coding: utf-8 -*-
from appium import webdriver

def setUp():
    desired_caps = {
        'platformName': 'iOS',
        'platformVersion': '13.3',
        #'platformVersion': '12.2',
        'appPackage': 'sg.gov.mnd.OneService',
        'deviceName': 'iPhone',
        #'deviceName': 'pccw_iPhoneXSMax',
        'udid': '4e7ec39ba0e997700a608b474c8d5c953cb53185',
        #'udid': '00008020-000A71EC22D8002E',
        'automationName': 'XCUITest',
        'noReset': 'true',
        'app': '/Users/Admin/Documents/Installer/ICMS (UAT).ipa',
        'appWaitForLaunch': 'true',
        "xcodeOrgid": "JGW7RRY73J",
        "xcodeSigningId": "iPhone Developer",
        'skipServerInstallation': 'true'
    }

    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)
    driver.implicitly_wait(3)
    return driver

if __name__ == '__main__':
    setUp()
