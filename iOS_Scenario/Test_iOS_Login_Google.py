import Test_iOS_capability
import time
import iOS_XLUtilities
from selenium.webdriver.support.ui import WebDriverWait

driver = Test_iOS_capability.setUp()
path = '//Users//Admin/Documents//Test Script//User_Login.xlsx'
Image_path = '//Users//kuanwah//Documents//Test Image//Artefacts.png'
google_account = 'MSO PCCW mso.osappinbox@gmail.com'

#Click google button
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id("btn googleLogin")):
        driver.find_element_by_accessibility_id("btn googleLogin").click()
except:
    pass

#Click "Continue" on the pop up message
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id("Continue")):
        driver.find_element_by_accessibility_id("Continue").click()
except:
    pass

#Select google account:
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id(google_account)):
        driver.find_element_by_accessibility_id(google_account).click()
except:
    pass

time.sleep(10)
#Click "Profile" icon
ele_profile_icon_btn = '//XCUIElementTypeButton[4]'
try:
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath(ele_profile_icon_btn)):
        driver.find_element_by_xpath(ele_profile_icon_btn).click()
except:
    pass

time.sleep(5)
driver.get_screenshot_as_file(Image_path)
loc1 = 'B9'
iOS_XLUtilities.InsertImg(path, loc1, Image_path)
print('Screenshot for user profile was attached...')

#Verify user id
Username = iOS_XLUtilities.readData(path, 'Info', 7, 3)
try:
    print(Username)
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id('nitram123')):
        ele_username = driver.find_element_by_accessibility_id('nitram123')
        actual_username = ele_username.get_attribute('name')
        print('Actual username: ' + actual_username)
        print('Expected username: ' + str(Username))
        if actual_username == Username:
            iOS_XLUtilities.writeData(path, "Info", 7, 2, actual_username)
            iOS_XLUtilities.writeData(path, 'Info', 7, 4, 'PASS')
        else:
            iOS_XLUtilities.writeData(path, 'Info', 7, 2, actual_username)
            iOS_XLUtilities.writeData(path, 'Info', 7, 4, 'FAILED')
except:
    iOS_XLUtilities.writeData(path, 'Info', 7, 4, 'FAILED')
    pass


#Verify user mobile number
mobileNum = iOS_XLUtilities.readData(path, 'Info', 8, 3)
try:
    print(mobileNum)
    if WebDriverWait(driver, 10).until(lambda x: x.find_element_by_accessibility_id('91722497')):
        ele_MobileNo = driver.find_element_by_accessibility_id('91722497')
        actual_mobileNo = ele_MobileNo.get_attribute('name')
        print('Actual Mobile Number: ' + actual_mobileNo)
        print('Expected Mobile Number: ' + str(mobileNum))
        if actual_mobileNo == mobileNum:
            iOS_XLUtilities.writeData(path, 'Info', 8, 2, actual_mobileNo)
            iOS_XLUtilities.writeData(path, 'Info', 8, 4, 'PASS')
        else:
            iOS_XLUtilities.writeData(path, 'Info', 8, 2, actual_mobileNo)
            iOS_XLUtilities.writeData(path, 'Info', 8, 4, 'FAILED')
except:
    iOS_XLUtilities.writeData(path, 'Info', 8, 4, 'FAILED')
    pass


#User logout
try:
    if WebDriverWait(driver, 20).until(lambda x: x.find_element_by_accessibility_id('Log Out')):
        driver.find_element_by_accessibility_id('Log Out').click()
except:
    pass

iOS_XLUtilities.close(path)
driver.quit()